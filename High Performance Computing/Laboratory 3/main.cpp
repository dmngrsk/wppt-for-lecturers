#include "pollard_rho_dlp.hpp"
#include "utilities.hpp"

int main(int argc, char* argv[])
{
    omp_set_num_threads(8);

    int L;
    NTL::ZZ p, a, b, c;

    // Read L. Defaults to 40.
    L = argc > 1 ? std::stoi(argv[1]) : 40;

    // Roll random p, a, b.
    p = FindStrongPrime(L);
    a = FindGroupGenerator(p);
    b = FindRandomElementOfGroup(p);
	
    // Find c such that a^c = b.
    c = PollardRhoDLP(NTL::ZZ(84324959), NTL::ZZ(84324959), NTL::ZZ(43196108), NTL::ZZ(13182341));
    if (c == -1) return -1;
	
    std::cout << a << "^" << c << " = " << b << " (mod " << p << ")" << std::endl;
    return 0;
}