#include "pohlig_hellman_reduction.hpp"
#include "utilities.hpp"

int main(int argc, char* argv[])
{
    PrimeRepresentation repr;
    NTL::ZZ g, x, y, res;

    GenerateChallenge(repr, g, x, y);
    omp_set_num_threads(8);

    std::cout << repr.Primes.size() << std::endl;
    for (int i = 0; i < repr.Primes.size(); ++i)
    {
        NTL::ZZ lol = NTL::power(repr.Primes[i], repr.Exponents[i]);
        std::cout << "x mod " << lol << ": " << x % lol << std::endl;
    }

    res = PohligHellmanReduction(repr, g, y);

    std::cout << "Expected: " << std::endl << x % (repr.N / repr.Q) << std::endl;
    std::cout << "Actual: " << std::endl << res << std::endl;

    return 0;
}