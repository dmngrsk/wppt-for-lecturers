#ifndef POHLIG_HELLMAN_REDUCTION_INCLUDED
#define POHLIG_HELLMAN_REDUCTION_INCLUDED

#include "utilities.hpp"
#include "../Laboratory 3/pollard_rho_dlp.hpp"
#include <iostream>
#include <NTL/ZZ.h>
#include <omp.h>
#include <vector>
#include <unordered_map>

struct PrimeRepresentation
{
    std::vector<NTL::ZZ> Primes;
    std::vector<int> Exponents;
    NTL::ZZ Q;
    NTL::ZZ N;
    NTL::ZZ P;
};

struct CongruentPoint
{
    NTL::ZZ X;
    NTL::ZZ P;
};

void GenerateChallenge(PrimeRepresentation &repr, NTL::ZZ &g, NTL::ZZ &x, NTL::ZZ &y);
NTL::ZZ PohligHellmanReduction(PrimeRepresentation repr, NTL::ZZ g, NTL::ZZ y);

#endif