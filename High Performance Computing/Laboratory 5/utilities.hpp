#ifndef UTILITIES_INCLUDED
#define UTILITIES_INCLUDED

#include <NTL/ZZ.h>
#include <algorithm>
#include <sstream>
#include <string>

NTL::ZZ FindRandomElementOfGroup(NTL::ZZ p);
std::string ZzToString(NTL::ZZ n);
std::string ZzToBinaryString(NTL::ZZ n);

#endif