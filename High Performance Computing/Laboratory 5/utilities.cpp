#include "utilities.hpp"

NTL::ZZ FindRandomElementOfGroup(NTL::ZZ p)
{
    return NTL::RandomBnd(p - 1) + NTL::ZZ(1);
}

std::string ZzToString(NTL::ZZ n)
{
    std::stringstream buffer;
    buffer << n;

    return buffer.str();
}

std::string ZzToBinaryString(NTL::ZZ n)
{
    std::stringstream buffer;
    NTL::ZZ copy = n;

    while (copy > 0)
    {
        buffer << (copy % 2);
        copy /= 2;
    }

    std::string res = buffer.str();
    std::reverse(res.begin(), res.end());
    
    return res;
}