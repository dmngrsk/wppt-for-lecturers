#include "pohlig_hellman_reduction.hpp"

void GenerateChallenge(PrimeRepresentation &repr, NTL::ZZ &g, NTL::ZZ &x, NTL::ZZ &y)
{
    while (true)
    {
        int k = NTL::RandomBnd(5) + 8;
        int powerBrowsingLimit = pow(5, k);

        repr.Q = NTL::GenPrime_ZZ(NTL::RandomBnd(32) + 256) * NTL::GenPrime_ZZ(NTL::RandomBnd(32) + 256);
        repr.Primes.push_back(NTL::ZZ(2));
        for (int i = 1; i < k; ++i) repr.Primes.push_back(NTL::GenGermainPrime_ZZ(NTL::RandomBnd(11) + 30));

        for (int i = 0; i < powerBrowsingLimit; ++i)
        {
            int browsePoint = i;

            repr.Exponents.clear();
            repr.P = repr.Q;

            for (int j = 0; j < k; ++j) 
            {
                repr.Exponents.push_back(browsePoint % 5 + 3);
                repr.P *= NTL::power(repr.Primes[j], repr.Exponents[j]);
                browsePoint /= 5;
            }

            repr.N = repr.P;
            
            if (++repr.P % 2 == 1 && !NTL::MillerWitness(repr.P, NTL::ZZ(32)))
            {
                g = FindRandomElementOfGroup(repr.P);
                x = FindRandomElementOfGroup(repr.P);
                y = NTL::PowerMod(g, x, repr.P);

                return;
            }
        }
    }
}


NTL::ZZ ExhaustiveDLP(NTL::ZZ p, NTL::ZZ q, NTL::ZZ g, NTL::ZZ y)
{
    for (int i = 0; i < q; ++i)
    {
        if (NTL::PowerMod(g, i, p) == y)
        {
            return NTL::ZZ(i);
        }
    }

    throw std::invalid_argument("Something went horribly wrong.");
}

NTL::ZZ SolveCongruences(std::vector<CongruentPoint> congruences)
{
    NTL::ZZ sum = NTL::ZZ(0);
    NTL::ZZ product = NTL::ZZ(1);

    for (int i = 0; i < congruences.size(); ++i) 
    {
        product *= congruences[i].P;
    }

    std::cout << product << std::endl << std::endl;

    for (int i = 0; i < congruences.size(); ++i) 
    {
        NTL::ZZ p = product / congruences[i].P;
        sum += congruences[i].X * NTL::InvMod(p % congruences[i].P, congruences[i].P) * p;
    }

    return sum % product;
}

NTL::ZZ PohligHellmanReduction(PrimeRepresentation repr, NTL::ZZ g, NTL::ZZ y)
{
    std::vector<CongruentPoint> congruences;
    NTL::ZZ x, q, e, a, b, c, l;

    // Find the congruence using exhaustive method for p0^e0. (p0 = 2)
    NTL::ZZ twoOrder = NTL::PowerMod(repr.Primes[0], repr.Exponents[0], repr.P);
    NTL::ZZ twoGenerator = NTL::PowerMod(g, repr.N / twoOrder, repr.P);
    NTL::ZZ twoElement = NTL::PowerMod(y, repr.N / twoOrder, repr.P);
    NTL::ZZ twoExponent = ExhaustiveDLP(repr.P, twoOrder, twoGenerator, twoElement);
    std::cout << "Found x: " << twoExponent << std::endl;
    congruences.push_back({twoExponent, twoOrder});

    // Find the congruences using Pollard-Rho method for p1^e1, ..., pk^ek. 
    for (int i = 1; i < repr.Primes.size(); ++i)
    {
        x = 0;
        q = repr.Primes[i];
        e = repr.Exponents[i];

        c = 1;
        l = 0;

        a = NTL::PowerMod(g, repr.N / q, repr.P);

        for (int j = 0; j < e; ++j)
        {
            c = j > 0 ? NTL::MulMod(c, NTL::PowerMod(g, NTL::MulMod(l, NTL::PowerMod(q, j - 1, repr.P), repr.P), repr.P), repr.P) : NTL::ZZ(1);
            b = NTL::PowerMod(NTL::MulMod(y, NTL::InvMod(c, repr.P), repr.P), repr.N / NTL::PowerMod(q, j + 1, repr.P), repr.P);
            // l = PollardRhoDLP(q, q, a % q, b % q);

            std::cout << q << ", digit " << j+1 << "/" << e << ": ";
            std::flush(std::cout);
            l = PollardRhoDLP(q, q, a % q, b % q);
            std::cout << l << std::endl;

            x += l * NTL::PowerMod(q, j, repr.P);
        }
        
        congruences.push_back({x, NTL::PowerMod(q, e, repr.P)});
    }

    return SolveCongruences(congruences);
}