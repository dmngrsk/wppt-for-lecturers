# Identification Systems, the final project report: Face Recognition.

**Author**: Damian Górski.

**Date**: January 24, 2019.

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Identification%20Systems). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## Overview

Let us start briefly by defining what is face detection, what is face recognition, and what is the difference between these two terms.

- **Face detection algorithms** are able of... well, detecting a face in a given image, basing on common features that are desired on a human's face. There is no distinguishing between two different people's faces - the output is merely an information that the image contains a face (and perhaps common features coming with it, such as eyes or ears) and where that face is located on the image.
- **Face recognition algorithms** make use of a face detection algorithm in order to detect a face on an image, extract features specific to a given person from their face, remember that information in something called a *model*, and finally use that *model* in order to recognize faces of people that the system has already seen.

In this report we will dive into the details of face recognition algorithms, and analyze the results of experiments on the yalefaces image set. We will use OpenCV as our Computer Vision library of choice, and the EmguCV wrapper for OpenCV in order to consume it in the .NET framework.

The first part of the report is a synopsis of what face recognition is in more detail. We will go through the steps required in order to recognize some faces, and explain the algorithms behind them. It will also include brief code snippets in the EmguCV wrapper.

The second part will be a summary of performed experiments. We will use the [Yale Face Database](http://vision.ucsd.edu/content/yale-face-database) and the [MUCT Face Database](http://www.milbo.org/muct/) as our datasets, split them into training and test sets, and finally evaluate FAR/FRR curves while playing with the parameters of face detection/recognition algorithms.

\pagebreak

## Theory behind face recognition

The process of face recognition can be briefly contained in the following four steps:

1. Face detection
2. Face preprocessing
3. Model training
4. Face recognition

We will cover them in more detail in the following subsections.

### Step 1: Face detection

Before the actual face recognition can take place, it first must be detected.

OpenCV comes with multiple comes pretrained XML detectors that one could use for different purposes. The following table contains the names for the most popular ones:

| Type of cascade classifier           | XML filename                          |
|--------------------------------------|---------------------------------------|
| Face detector (default)              | `haarcascade_frontalface_default.xml` |
| Face detector (fast Haar)            | `haarcascade_frontalface_alt2.xml`    |
| Face detector (fast LBP)             | `lbpcascade_frontalface.xml`          |
| Profile (side-looking) face detector | `haarcascade_profileface.xml`         |
| Eye (separate for left and right)    | `haarcascade_lefteye_2splits.xml`     |
| Mouth detector                       | `haarcascade_mcs_mouth.xml`           |
| Nose detector                        | `haarcascade_mcs_nose.xml`            |
| Whole person                         | `detector haarcascade_fullbody.xml`   |

We will use the first one, the Haar cascade face detector which is a detector that was discovered in 2001, and then further improved until 2007. It is able to recognize faces in real-time, with a high accuracy. It was trained using ~1000 unique face images of actual people and ~10000 images with no faces at all.

The basic idea behind the Haar-based face detector is comparing the brightness between multiple places, for example: the region with the eyes should be darker than the forehead and cheeks, and the region with the mouth should be darker than cheeks, and so on. There usually is about 20 stages of comparisons like this to decide if a given region of an image is a face or not, and it checks for faces in a cascade way (for each given valid face region size larger than the minimal one, check every valid region that's inside the boundaries of the image, for example: for a 5x5 image and minimal region of 3x3, there are two 4x5 regions, two 5x4 regions, four 4x4 regions, six 3x4 regions, six 4x3 regions, nine 3x3 regions, and every single one of them could be a face).

![Nobody is perfect. Neither is the Haar cascade face detector.](./img/haar-fail.png)

The following C# code snippet uses `OpenCV` and `EmguCV` in order to detect faces in an image that's located at `imagePath`:

```c#
// Factor of sizes to search from;
// the bigger the factor, the smaller the accuracy.
const double scaleFactor = 1.1;
// Amount of features that should match;
// the bigger the factor, the smaller the FAR.
const int minNeighbours = 10;
 // Minimal face size in pixels.
static readonly Size minFaceSize = new Size(20, 20);

var classifierFileName = "haarcascade_frontalface_default.xml";

using (var src = new UMat(imagePath, ImreadModes.Color))
using (var dest = new UMat())
using (var faceClassifier = new CascadeClassifier(classifierFileName))
{
    // Convert the image to grayscale image,
    // because the classifier won't work otherwise.
    CvInvoke.CvtColor(src, dest, ColorConversion.Bgr2Gray);

    // Shrink the image if it's too large.
    var scale = Math.Max((float)DetectionWidth / src.Size.Width, 1.0f);
    var scaledSize = ScaleSize(src.Size, scale);
    if (scale < 1.0f) CvInvoke.Resize(dest, dest, scaledSize);

    // Perform histogram equalization in order
    // to improve the contrast and brightness.
    CvInvoke.EqualizeHist(dest, dest);

    // Detect faces on the image using above parameters
    // (also rescale the result rectangles).
    var facesDetected = faceClassifier
        .DetectMultiScale(dest, scaleFactor, minNeighbours, minFaceSize)
        .Select(r => ScaleRectangle(r, 1.0f / scale));

    // Do something with the face rectangles at this point...
}
```

### Part 2: Face preprocessing

Before the model is taught, a face preprocessing is required. There are multiple ways to do that, depending on the circumstances in which the data set photos are taken (most notable factors are the lightning, face orientation, face expression). It's important to reduce the difference factors as much as possible.

If one would want to implement a better face preprocessor, he could use the features extracted with use of other cascade detectors mentioned above, perform a geometrical transformation of the face, perform histogram equalization for separate features, merge and smoothen the image, and finally transform the image into a form of an elliptical mask (the face is inside an image, which borders form an egg-like shape).

We are going to base on a dataset with photos taken in roughly the same environment, so we could go with the easiest approach, which is solely applying histogram equalization (already done in the previous step, so we basically omit the face preprocessing step).

### Part 3: Model training

In order to train a model, a *training set* is required. Each entry in this set should be a tuple $(P, i)$, where $P$ is a picture containing a person, and $i$ is a *label* pointing to that person. Naturally, every photo with a label $i$ should point to the same person. The amount of correct labelings should be as large as possible (perfectly 100%). It is also very important to normalize the image into the form of a preprocessed face, as mentioned in the previous step.

After preparing a set of normalized, preprocessed faces, now we will learn our model. This is a common word used in machine learning - *a model*. What exactly is a model, though? According to ISO/IEC 2382-37, a biometric model is a *stored function generated from biometric data*. The way this function is constructed depends on the face recognition algorithm one would want to use. We will focus on two: Eigenfaces and Fisherfaces, and briefly describe them here.

In simple terms, the basic principle of the Eigenfaces algorithm is that it calculates a set of special, "averaged" images, called *eigenfaces*, and their corresponding blending ratios, called *eigenvalues*. These two values combined in different ways can generate each of the images in the training set, but also can be used to differentiate many faces images in the training set from each other. Eigenvalues are basically features; for example, if some faces have a moustache, they would have a high blending ratio (eigenvalue) that tells a face has a moustache, and low blending ratio if they don't. In this way, in the validation step the algorithm looks at all the faces as a whole and extracts only the components which are relevant for face recognition. These important features are called *principal components*. Naturally, the face with the best match ratio is the one that's selected as the output for the algorithm.

The drawback of the Eigenfaces approach is that it look at all the faces at the same time. Especially in scenarios in which people's faces are illuminated differently, it will find the illumination itself as an important feature and consinder other, actually more relevant features as less important. In order to fix that, the Fisherfaces algorithm extracts features from each person's labelled face images separately, instead of looking at all of them at once. In this way, the algorithm is able to specify differences between certain people (which features of that person's face are more important, and which are less).

There is also an another recognizer in this family, referred to as the Local Binary Pattern Histograms (LBPH) face recognizer, but we omit it in this report.

For a more detailed comparison with the computational logic behind Eigenfaces and Fisherfaces (*Principal Component Analysis* for Eigenfaces, and *Linear Discriminant Analysis* for Fisherfaces), the reader is advised to read the following paper by Pablo Navarrete and Javier Ruiz-del-Solar, called [*Comparative Study between different Eigenspace-based
Approaches for Face Recognition* (click!)](https://www.cec.uchile.cl/~aabdie/jruizd/papers/afss2002b.pdf).

The following code snippet takes a collection of labelled, preprocessed images, and uses them as an input to the `void Train(imgs, lbls)` method.

```c#
var images = new List<Image<Gray, byte>>();
var labels = new List<int>();

foreach (var group in imageGroups)
{
    var label = group.Key;

    foreach (var image in group)
    {
        // Code from Step 1 wrapped into a class
        var faceDetectionResult = _faceDetectionService.DetectFaces(imagePath);

        // Avoiding anomalies
        if (faceDetectionResult.Faces.Count != 1) continue;

        var faceImage = image
            .GetSubRect(faceDetectionResult.Faces.Single())
            .Resize(60, 60, Inter.Linear); // Normalizing to same size

        images.Add(faceImage);
        labels.Add(label);
    }
}

_faceRecognizer.Train(images.ToArray(), labels.ToArray());
```

At this point, the `_faceRecognizer` object contains the collection of eigenfaces and their corresponding eigenvalues, which together build the model. One could serialize that model to an `.xml` file, and then deserialize it in a fairly easy way, so it is not hard to preserve the model in between process executions.

```c#
_faceRecognizer.Write(xmlPath);

// Process restarts...

_faceRecognizer.Load(xmlPath);
```

### Part 4: Face recognition

One we have the algorithm trained, set up and ready, it's about time to recognize some faces. The very same process applies to the test image as the training image - it has to be normalized into a set of eigenfaces and eigenvalues. Then, these calculated values are compared with these in the model, using the heuristics described above.

The result of a recognition is an array of *predictions*. Each prediction is a tuple $(i, d)$, where $i$ denotes one of previously defined labels, and $d$ denotes the *distance* between the face on the source image and the face that the model thinks is similar to it. There could be multiple face predictions for a source image - this comes with the fact that some people have similar faces by nature (especially relatives). Obviously, when given an image from the training set, the face recognizer will return the corresponding label with a distance of 0, because the model remembers every single feature of that face.

![An example result of face recognition.](./img/recognition-predictions.png)

The following code snippet uses the trained `_faceRecognizer` object in order to find the predictions:

```c#
var predictions = faceRectangles // Obtained in Step 1
    .Select(face => image.GetSubRect(face).Resize(60, 60, Inter.Linear)) // Normalizing
    .Select(faceImage => _faceRecognizer.Predict(faceImage)) // Prediction
    .OrderBy(p => p.Distance);
```

From there, there are many things to explore: one could try to measure how similar people in the dataset are (fe. form a fully-connected weighted graph and study the weights), or evaluate FAR/FRR curves in order to check how well the proposed system could serve as an identification system. We will focus on the last thing in the next section.

\pagebreak

## Experiment: evaluating FAR/FRR for Eigenfaces and Fisherfaces with different parameters

We performed several experiments using two image databases:

- [The Yale Face Database](http://cvc.cs.yale.edu/cvc/projects/yalefaces/yalefaces.html), which contains 165 photos (15 people in 11 different expressions)
- [The MUCT Face Database](http://www.milbo.org/muct/), which contains 3755 photos (276 people in various amount of different expressions)

The main goal of the experiments is to measure the reliability of the Eigenface and Fisherface methods with different Haar classifier's parameters on both little and large data sets, and to find any correlations in these results.

The OpenCV library returns just a single face prediction for each face, so we will assume that the identification systems returns `true` if the label predicted by the network is equal to the label of a person that's trying to identify, and `false` otherwise.

The total FRR is, in this case, the amount of unsuccessful identifications divided by the total amount of identifications. If OpenCV returned more results (like two or more predictions), perhaps the analysis would be more interesting.

The total FAR is the amount of unsuccessful identifications **pointing to an another person** divided by the total amount of identifications. Sometimes the network will not return any predictions, or return a prediction with label `-1`, which likely means a face has been recognized, but it's not close enough (in terms of distance) to the faces in the model, or just an error. For some experiments the system has always returned a prediction, so sometimes the FAR would be equal to the FRR.

All the experiments (except for the last one) were performed using the Yale Faces Database as the data set. We denote the experiment as `AlgorithmName(scale_factor, min_neighbours, num_components, distance_threshold)`, where `AlgorithmName` is eiter Eigenfaces or Fisherfaces, `scale_factor` and `min_neighbours` are the parameters for the Haar classifier for face detection, and `num_components` and `distance_threshold` are the parameters for the respective face recognition algorithm. Every model was trained using 80% of the data and tested with remaining 20% of the data.

The template experiment parameters are `{Eigen,Fisher}faces(1.1, 4, 32, +Infinity)`; we will modify every single parameter and see what happens.

### Scale factor in the classifier

Increasing the scale factor increases the FRR, but does not increase the FAR - this is because the algorithm performs less face checks, and sometimes fails to recognize a face.

| Experiment                            |  FAR  |  FRR  |
|---------------------------------------|-------|-------|
| `Eigenfaces(1.01, 4, 32, +Infinity)`  | 30,0% | 30,0% |
| `Eigenfaces(1.1, 4, 32, +Infinity)`   | 26,7% | 26,7% |
| `Eigenfaces(1.25, 4, 32, +Infinity)`  | 20,0% | 23,3% |
| `Eigenfaces(1.5, 4, 32, +Infinity)`   | 16,7% | 33,3% |
| `Fisherfaces(1.01, 4, 32, +Infinity)` | 13,3% | 13,3% |
| `Fisherfaces(1.1, 4, 32, +Infinity)`  | 13,3% |  6,7% |
| `Fisherfaces(1.25, 4, 32, +Infinity)` | 16,7% | 23,3% |
| `Fisherfaces(1.5, 4, 32, +Infinity)`  | 16,7% | 27,7% |

### Minimum neighbours in the classifier

Increasing that number increases the general accuracy (lower FAR/FRR). However when going overboard, the FRR starts to increase again because the face recognizing algorithm fails to find matches that are close to the received image (not enough neighbouring features found).

| Experiment                            |  FAR  |  FRR  |
|---------------------------------------|-------|-------|
| `Eigenfaces(1.1, 2, 32, +Infinity)`   | 36,7% | 36,7% |
| `Eigenfaces(1.1, 4, 32, +Infinity)`   | 23,3% | 23,3% |
| `Eigenfaces(1.1, 8, 32, +Infinity)`   | 26,7% | 26,7% |
| `Eigenfaces(1.1, 16, 32, +Infinity)`  | 20,0% | 26,7% |
| `Eigenfaces(1.1, 32, 32, +Infinity)`  | 16,7% | 40,0% |
| `Fisherfaces(1.1, 2, 32, +Infinity)`  | 16,7% | 16,7% |
| `Fisherfaces(1.1, 4, 32, +Infinity)`  |  6,7% |  6,7% |
| `Fisherfaces(1.1, 8, 32, +Infinity)`  |  6,7% | 10,0% |
| `Fisherfaces(1.1, 16, 32, +Infinity)` |  6,7% | 13,3% |
| `Fisherfaces(1.1, 32, 32, +Infinity)` |  3,3% | 20,0% |

### Number of components in the recognizer

Both the FAR and FRR are higher when given a small amount of components to remember, and they stop increasing at some point. The biggest factor to take into consideration when selecting the number of remembered components is the performance and size of the model.

| Experiment                            |  FAR  |  FRR  |
|---------------------------------------|-------|-------|
| `Eigenfaces(1.1, 4, 2, +Infinity)`    | 60.0% | 60.0% |
| `Eigenfaces(1.1, 4, 8, +Infinity)`    | 36,7% | 36,7% |
| `Eigenfaces(1.1, 4, 32, +Infinity)`   | 26,7% | 26,7% |
| `Eigenfaces(1.1, 4, 128, +Infinity)`  | 20,0% | 20,0% |
| `Fisherfaces(1.1, 4, 2, +Infinity)`   | 36,7% | 40,7% |
| `Fisherfaces(1.1, 4, 8, +Infinity)`   | 26,7% | 26,7% |
| `Fisherfaces(1.1, 4, 32, +Infinity)`  |  6,7% |  6,7% |
| `Fisherfaces(1.1, 4, 128, +Infinity)` |  6,7% |  6,7% |

### Distance threshold in the recognizer

Similarly to the minimum number of neighbours - the bigger the restriction, the larger FRR.

| Experiment                           |  FAR  |  FRR  |
|--------------------------------------|-------|-------|
| `Eigenfaces(1.1, 4, 32, +500.0)`     |  0,0% | 67,7% |
| `Eigenfaces(1.1, 4, 32, +1000.0)`    |  3,3% | 43,3% |
| `Eigenfaces(1.1, 4, 32, +2500.0)`    | 20,0% | 30,0% |
| `Eigenfaces(1.1, 4, 32, +5000.0)`    | 23,3% | 26,7% |
| `Eigenfaces(1.1, 4, 32, +Infinity)`  | 26,7% | 26,7% |
| `Fisherfaces(1.1, 4, 32, +500.0)`    |  0,0% | 36,7% |
| `Fisherfaces(1.1, 4, 32, +1000.0)`   |  0,0% | 16,7% |
| `Fisherfaces(1.1, 4, 32, +2500.0)`   | 10,0% | 10,0% |
| `Fisherfaces(1.1, 4, 32, +5000.0)`   | 10,0% | 10,0% |
| `Fisherfaces(1.1, 4, 32, +Infinity)` |  6,7% |  6,7% |

### Eigenfaces vs Fisherfaces

From the tables above, one could notice that the Fisherfaces algorithm works with better overall results (lower FAR/FRR). It is to be expected, as Fisherfaces are an improvement over Eigenfaces. However, after performing the test on a much larger data set (MUCT), the results were surprising:

| Experiment                           |  FAR  |  FRR  |
|--------------------------------------|-------|-------|
| `Eigenfaces(1.1, 4, 32, +Infinity)`  | 43,0% | 44,7% |
| `Fisherfaces(1.1, 4, 32, +Infinity)` | 86,3% | 88,5% |

Even though the data are preprocessed in the same way, the Fisherfaces algorithm starts to fail in models which contain over 100 faces. The success rate is still significantly larger than a random guess, but it is performing terribly in comparison to the Eigenfaces algorithm.

## Summary

While Eigenface and Fisherface algorithms do their work with a better-than-random probability, it is a deprecated solution. There are many new approaches, such as OpenFace (available on GitHub), which uses convolutional neural networks with SVM-s, and that approach yields much more accuracy. It doesn't mean we shouldn't analyze the old approaches, because they're interesting in their way of working as well. However, if I were to prepare a face recognition system for commercial use, I would definitely choose modern solutions over the ones I have just described.