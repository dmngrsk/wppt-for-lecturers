﻿namespace Speculo.Core.Enums
{
    public enum DatasetEntryType
    {
        Training,
        Test
    }
}
