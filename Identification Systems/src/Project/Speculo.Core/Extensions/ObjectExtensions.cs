﻿using Newtonsoft.Json;

namespace Speculo.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static string Serialize(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }
    }
}
