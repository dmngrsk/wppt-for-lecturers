﻿namespace Speculo.Core.Models.ComputerVision
{
    public class FaceRecognitionResult
    {
        public int Label { get; set; }
        public double Distance { get; set; }

        public FaceRecognitionResult(Emgu.CV.Face.FaceRecognizer.PredictionResult prediction)
        {
            Label = prediction.Label;
            Distance = prediction.Distance;
        }
    }
}
