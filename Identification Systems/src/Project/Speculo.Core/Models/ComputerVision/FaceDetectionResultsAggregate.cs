﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Speculo.Core.Models.ComputerVision
{
    public class FaceDetectionResultsAggregate
    {
        public IReadOnlyCollection<FaceDetectionResult> Faces { get; }

        public FaceDetectionResultsAggregate(IReadOnlyCollection<FaceDetectionResult> faces)
        {
            Faces = faces;
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
