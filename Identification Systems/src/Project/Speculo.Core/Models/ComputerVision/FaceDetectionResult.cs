﻿using System.Drawing;

namespace Speculo.Core.Models.ComputerVision
{
    public class FaceDetectionResult
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public FaceDetectionResult(Rectangle rect)
        {
            X = rect.X;
            Y = rect.Y;
            Width = rect.Width;
            Height = rect.Height;
        }
    }
}
