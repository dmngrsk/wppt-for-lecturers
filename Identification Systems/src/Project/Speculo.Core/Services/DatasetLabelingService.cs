﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Speculo.Core.Services
{
    public class DatasetLabelingService
    {
        private int _counter;
        private string _last;

        public ILookup<int, string> LabelDataset(IEnumerable<string> imagePaths)
        {
            _counter = 0;
            _last = null;

            return imagePaths
                .GroupBy(Path.GetDirectoryName)
                .OrderBy(x => x.Key)
                .SelectMany(xs => xs.Select(x => new { Label = GetLabel(xs.Key), ImagePath = x }))
                .ToLookup(x => x.Label, x => x.ImagePath);
        }

        private int GetLabel(string current)
        {
            if (current == _last) return _counter;

            _last = current;
            return ++_counter;
        }
    }
}
