﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Speculo.Core.Services
{
    public class LookupJsonSerializerService<TK, TV>
    {
        public string Serialize(ILookup<TK, TV> lookup)
        {
            return JsonConvert.SerializeObject(lookup.ToDictionary(x => x.Key, x => x.Select(y => y).ToList()));
        }

        public ILookup<TK, TV> Deserialize(string json)
        {
            return JsonConvert
                .DeserializeObject<Dictionary<TK, IReadOnlyCollection<TV>>>(json)
                .SelectMany(x => x.Value.Select(y => new Tuple<TK, TV>(x.Key, y)))
                .ToLookup(x => x.Item1, x => x.Item2);
        }
    }
}
