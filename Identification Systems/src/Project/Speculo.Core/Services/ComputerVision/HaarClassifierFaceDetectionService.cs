﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using NLog;
using Speculo.Core.Interfaces.Services.ComputerVision;
using Speculo.Core.Models.ComputerVision;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Speculo.Core.Services.ComputerVision
{
    public class HaarClassifierFaceSize
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public HaarClassifierFaceSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    public class HaarClassifierFaceDetectionService : IFaceDetectionService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const int DetectionWidth = 320;

        private readonly float _scaleFactor;
        private readonly int _minNeighbours;
        private readonly Size _minimalFaceSize;

        public HaarClassifierFaceDetectionService() : this(1.1f, 4, new HaarClassifierFaceSize(20, 20))
        {
        }

        public HaarClassifierFaceDetectionService(float scaleFactor, int minNeighbours, HaarClassifierFaceSize minimalFaceSize)
        {
            _scaleFactor = scaleFactor;
            _minNeighbours = minNeighbours;
            _minimalFaceSize = new Size(minimalFaceSize.Width, minimalFaceSize.Height);
        }

        public FaceDetectionResultsAggregate DetectFaces(string imagePath)
        {
            var faces = new List<Rectangle>();
            Logger.Trace($"Detecting faces from image {imagePath}...");

            using (var src = new UMat(imagePath, ImreadModes.Color))
            using (var dest = new UMat())
            using (var faceClassifier = new CascadeClassifier("./Detectors/haarcascade_frontalface_default.xml"))
            {
                CvInvoke.CvtColor(src, dest, ColorConversion.Bgr2Gray);

                var scale = Math.Max((float)DetectionWidth / src.Size.Width, 1.0f);
                var scaledSize = ScaleSize(src.Size, scale);
                if (scale < 1.0f) CvInvoke.Resize(dest, dest, scaledSize);

                CvInvoke.EqualizeHist(dest, dest);

                var facesDetected = faceClassifier
                    .DetectMultiScale(dest, _scaleFactor, _minNeighbours, _minimalFaceSize)
                    .Select(r => scale < 1.0f ? ScaleRectangle(r, 1.0f / scale) : r);

                faces.AddRange(facesDetected);
            }

            return new FaceDetectionResultsAggregate(faces.Select(r => new FaceDetectionResult(r)).ToList());
        }

        private static Size ScaleSize(Size size, float scale)
        {
            return new Size
            (
                (int)(size.Width * scale),
                (int)(size.Height * scale)
            );
        }

        private static Rectangle ScaleRectangle(Rectangle rect, float scale)
        {
            return new Rectangle
            (
                (int)(rect.X * scale),
                (int)(rect.Y * scale),
                (int)(rect.Width * scale),
                (int)(rect.Height * scale)
            );
        }
    }
}
