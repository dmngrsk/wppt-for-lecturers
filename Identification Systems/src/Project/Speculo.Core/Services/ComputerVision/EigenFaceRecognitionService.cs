﻿using Emgu.CV.Face;
using Speculo.Core.Interfaces.Services.ComputerVision;

namespace Speculo.Core.Services.ComputerVision
{
    public class EigenFaceRecognitionService : FaceRecognitionServiceBase
    {
        public EigenFaceRecognitionService(IFaceDetectionService faceDetectionService)
            : base(faceDetectionService, new EigenFaceRecognizer())
        {
        }

        public EigenFaceRecognitionService(IFaceDetectionService faceDetectionService, int numComponents, double threshold)
            : base(faceDetectionService, new EigenFaceRecognizer(numComponents, threshold))
        {
        }
    }
}
