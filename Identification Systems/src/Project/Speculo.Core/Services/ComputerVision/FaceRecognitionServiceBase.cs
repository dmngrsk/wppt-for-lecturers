﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using NLog;
using Speculo.Core.Interfaces.Services.ComputerVision;
using Speculo.Core.Models.ComputerVision;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Speculo.Core.Services.ComputerVision
{
    public abstract class FaceRecognitionServiceBase : IFaceRecognitionService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IFaceDetectionService _faceDetectionService;
        private readonly FaceRecognizer _faceRecognizer;

        protected FaceRecognitionServiceBase(IFaceDetectionService faceDetectionService, FaceRecognizer faceRecognizer)
        {
            _faceDetectionService = faceDetectionService;
            _faceRecognizer = faceRecognizer;
        }

        public IFaceRecognitionService TrainFromImages(ILookup<int, string> imagePathGroups)
        {
            var images = new List<Image<Gray, byte>>();
            var labels = new List<int>();

            Logger.Info("Starting learning process (training from images).");

            foreach (var group in imagePathGroups)
            {
                var label = group.Key;

                foreach (var imagePath in group)
                {
                    var image = new Image<Gray, byte>(imagePath);
                    Logger.Trace($"Extracted face from image {imagePath}.");

                    var faceDetectionResult = _faceDetectionService.DetectFaces(imagePath);
                    if (!faceDetectionResult.Faces.Any()) continue;

                    var face = faceDetectionResult.Faces.OrderByDescending(x => x.X).Take(1).Single();
                    var rect = new Rectangle(face.X, face.Y, face.Width, face.Height);

                    var faceImage = image
                        .GetSubRect(rect)
                        .Resize(60, 60, Inter.Linear);

                    images.Add(faceImage);
                    labels.Add(label);
                }
            }

            Logger.Info("Starting training, this might take a while...");

            _faceRecognizer.Train(images.ToArray(), labels.ToArray());
            Logger.Info("Model trained successfully.");

            return this;
        }

        public IFaceRecognitionService TrainFromXml(string xmlPath)
        {
            Logger.Info($"Loading learned model from {xmlPath}.");

            _faceRecognizer.Read(xmlPath);
            Logger.Info("Model loaded successfully.");

            return this;
        }

        public void SaveToXml(string xmlPath)
        {
            Logger.Info($"Saving learned model to {xmlPath}.");

            _faceRecognizer.Write(xmlPath);
            Logger.Info("Model saved successfully.");
        }

        public FaceRecognitionResult RecognizeFace(string imagePath)
        {
            Logger.Trace($"Recognizing image {imagePath}...");

            var image = new Image<Gray, byte>(imagePath);

            var face = _faceDetectionService
                .DetectFaces(imagePath)
                .Faces
                .OrderByDescending(x => x.X)
                .FirstOrDefault();

            if (face == null) return null;

            return new FaceRecognitionResult(_faceRecognizer
                .Predict(image
                .GetSubRect(new Rectangle(face.X, face.Y, face.Width, face.Height))
                .Resize(60, 60, Inter.Linear)));
        }
    }
}
