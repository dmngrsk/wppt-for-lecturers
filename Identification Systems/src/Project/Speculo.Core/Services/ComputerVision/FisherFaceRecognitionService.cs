﻿using Emgu.CV.Face;
using Speculo.Core.Interfaces.Services.ComputerVision;

namespace Speculo.Core.Services.ComputerVision
{
    public class FisherFaceRecognitionService : FaceRecognitionServiceBase
    {
        public FisherFaceRecognitionService(IFaceDetectionService faceDetectionService)
            : base(faceDetectionService, new FisherFaceRecognizer())
        {
        }

        public FisherFaceRecognitionService(IFaceDetectionService faceDetectionService, int numComponents, double threshold)
            : base(faceDetectionService, new FisherFaceRecognizer(numComponents, threshold))
        {
        }
    }
}
