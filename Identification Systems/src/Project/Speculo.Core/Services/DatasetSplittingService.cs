﻿using Speculo.Core.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Speculo.Core.Services
{
    public class DatasetSplittingService
    {
        public ILookup<DatasetEntryType, string> SplitDataset(string imagesDirectoryPath, float testSetSize)
        {
            var directories = Directory.GetDirectories(imagesDirectoryPath);
            var tuples = new List<Tuple<DatasetEntryType, string>>();

            foreach (var directory in directories)
            {
                var files = Directory.GetFiles(directory).OrderBy(_ => Guid.NewGuid()).ToList();
                var testCount = Convert.ToInt32(decimal.Round(Convert.ToDecimal(files.Count * testSetSize)));

                tuples.AddRange(files.Take(testCount).Select(f => new Tuple<DatasetEntryType, string>(DatasetEntryType.Test, f)));
                tuples.AddRange(files.Skip(testCount).Select(f => new Tuple<DatasetEntryType, string>(DatasetEntryType.Training, f)));
            }

            return tuples.ToLookup(t => t.Item1, t => t.Item2);
        }
    }
}
