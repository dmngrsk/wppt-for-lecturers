﻿using NLog;

namespace Speculo.Core.Utilities
{
    public class GenericLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Trace(string message)
        {
            Logger.Trace(message);
        }

        public void Info(string message)
        {
            Logger.Info(message);
        }
    }
}
