﻿using Speculo.Core.Models.ComputerVision;

namespace Speculo.Core.Interfaces.Services.ComputerVision
{
    public interface IFaceDetectionService
    {
        FaceDetectionResultsAggregate DetectFaces(string imagePath);
    }
}
