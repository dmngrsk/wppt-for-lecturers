﻿using Speculo.Core.Models.ComputerVision;
using System.Linq;

namespace Speculo.Core.Interfaces.Services.ComputerVision
{
    public interface IFaceRecognitionService
    {
        IFaceRecognitionService TrainFromImages(ILookup<int, string> imagePathGroups);
        IFaceRecognitionService TrainFromXml(string xmlPath);
        void SaveToXml(string xmlPath);
        FaceRecognitionResult RecognizeFace(string imagePath);
    }
}
