open System.IO

let inputPath = "./input"

let moveFile label src =
    let dest = sprintf "./%s/%s" label (Path.GetFileName(src))
    File.Move(src, dest)

let createDirectory (label, imgs) =
    let parentDirectory = sprintf "./%s" label
    printfn "%s" parentDirectory
    Directory.CreateDirectory(parentDirectory) |> ignore
    List.iter (fun img -> moveFile label img) imgs

Directory.GetFiles(inputPath)
|> List.ofArray
|> List.groupBy (fun (s:string) -> Path.GetFileName(s).Substring(0, 4))
|> List.iter createDirectory