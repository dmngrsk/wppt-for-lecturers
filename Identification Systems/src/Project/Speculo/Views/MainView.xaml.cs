﻿using Speculo.Core.Enums;
using Speculo.Core.Extensions;
using Speculo.Core.Interfaces.Services.ComputerVision;
using Speculo.Core.Services;
using Speculo.Core.Services.ComputerVision;
using Speculo.Core.Utilities;
using Speculo.ViewModels;
using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;

namespace Speculo.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : Window
    {
        private readonly IFaceRecognitionService _faceRecognitionService;
        private readonly GenericLogger _genericLogger;
        private readonly MainViewModel _viewModel;

        public MainView()
        {
            InitializeComponent();
            SourceInitialized += Window_SourceInitialized;

            _faceRecognitionService = new EigenFaceRecognitionService(new HaarClassifierFaceDetectionService());
            _genericLogger = new GenericLogger();
            _viewModel = (MainViewModel)this.DataContext;
            _viewModel.WindowTitle = $"Speculo (using {_faceRecognitionService.GetType().Name})";
        }

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;
        private const int WS_MAXIMIZEBOX = 0x10000;

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            var hwnd = new WindowInteropHelper((Window)sender).Handle;
            var value = GetWindowLong(hwnd, GWL_STYLE);
            SetWindowLong(hwnd, GWL_STYLE, (int)(value & ~WS_MAXIMIZEBOX));
        }

        private void TrainImageButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = "./";
                dialog.RootFolder = Environment.SpecialFolder.UserProfile;

                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                var dataset = new DatasetSplittingService().SplitDataset(dialog.SelectedPath, 0.2f);
                var trainingDataset = dataset[DatasetEntryType.Training];
                var testDataset = dataset[DatasetEntryType.Test];
                var labelizedDataset = new DatasetLabelingService().LabelDataset(trainingDataset);

                _faceRecognitionService.TrainFromImages(labelizedDataset);
                _genericLogger.Info($"Test dataset images: {testDataset.Serialize()}");

                _viewModel.IsTrained = true;
                _viewModel.TextBoxContents = MainViewModel.SelectImagePrompt;
            }
        }

        private void TrainXmlButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.InitialDirectory = "./";
                dialog.RestoreDirectory = true;
                dialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                _faceRecognitionService.TrainFromXml(dialog.FileName);

                _viewModel.IsTrained = true;
                _viewModel.TextBoxContents = MainViewModel.SelectImagePrompt;
            }
        }

        private void SelectImageButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.InitialDirectory = "./";
                dialog.RestoreDirectory = true;
                dialog.Filter = "JPG files (*.jpg)|*.jpg|PNG files (*.png)|*.png|GIF files (*.gif)|*.gif|PGM files (*.pgm)|*.pgm|All files (*.*)|*.*";
                dialog.FilterIndex = 5;

                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                var result = _faceRecognitionService.RecognizeFace(dialog.FileName);

                _viewModel.DisplayedImagePath = dialog.FileName;
                _viewModel.TextBoxContents = result.Serialize();
            }
        }

        private void SaveXmlButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.InitialDirectory = "./";
                dialog.RestoreDirectory = true;
                dialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
                dialog.FilterIndex = 1;

                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                _faceRecognitionService.SaveToXml(dialog.FileName);
            }
        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
