﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Speculo.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public static string TrainModelPrompt = "Train a model...";
        public static string SelectImagePrompt = "Select an image...";

        private string _windowTitle;
        public string WindowTitle
        {
            get => _windowTitle;
            set { _windowTitle = value; OnPropertyChanged(nameof(WindowTitle)); }
        }

        private string _displayedImagePath;
        public string DisplayedImagePath
        {
            get => _displayedImagePath;
            set { _displayedImagePath = value; OnPropertyChanged(nameof(DisplayedImagePath)); }
        }

        private string _textBoxContents;
        public string TextBoxContents
        {
            get => _textBoxContents;
            set { _textBoxContents = value; OnPropertyChanged(nameof(TextBoxContents)); }
        }

        private bool _isTrained;
        public bool IsTrained
        {
            get => _isTrained;
            set { _isTrained = value; OnPropertyChanged(nameof(IsTrained)); OnPropertyChanged(nameof(IsNotTrained)); }
        }
        public bool IsNotTrained
        {
            get => !_isTrained;
            set { _isTrained = !value; OnPropertyChanged(nameof(IsTrained)); OnPropertyChanged(nameof(IsNotTrained)); }
        }

        public MainViewModel()
        {
            TextBoxContents = TrainModelPrompt;
            IsTrained = false;
        }
    }
}
