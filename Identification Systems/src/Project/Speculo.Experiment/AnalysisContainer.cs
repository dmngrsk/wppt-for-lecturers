﻿using System.Collections.Generic;

namespace Speculo.Experiment
{
    public class AnalysisContainer
    {
        public float TotalFalseAcceptanceRate { get; set; }
        public float TotalFalseRejectionRate { get; set; }
        public ExperimentParameters Parameters { get; set; }
        public IReadOnlyCollection<AnalysisEntry> Entries { get; set; }
    }
}
