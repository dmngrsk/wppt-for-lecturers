﻿#define TRAINING

using Newtonsoft.Json;
using Speculo.Core.Services;
using Speculo.Core.Services.ComputerVision;
using System.IO;
using System.Linq;
using NLog;
using Speculo.Core.Interfaces.Services.ComputerVision;

#if TRAINING
using Speculo.Core.Enums;
#endif

namespace Speculo.Experiment
{
    public class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void Main(string[] args)
        {
            var experimentTemplate = new ExperimentParameters
            {
                ScaleFactor = 1.1f,
                MinNeigbours = 4,
                NumComponents = 32,
                DistanceThreshold = float.MaxValue
            };

            var experimentParametersCollection = Enumerable.Empty<ExperimentParameters>()
                .Concat(new[] { 1.01f, 1.1f, 1.25f, 1.5f }.Select(f => experimentTemplate.CloneWithValues(experimentName: "ScaleFactor", scaleFactor: f)))
                .Concat(new[] { 2, 4, 8, 16, 32 }.Select(mn => experimentTemplate.CloneWithValues(experimentName: "MinNeighbours", minNeighbours: mn)))
                .Concat(new[] { 2, 8, 32, 128 }.Select(nc => experimentTemplate.CloneWithValues(experimentName: "NumComponents", numComponents: nc)))
                .Concat(new[] { 500.0f, 1000.0f, 2500.0f, 5000.0f, float.MaxValue }.Select(dt => experimentTemplate.CloneWithValues(experimentName: "DistanceThreshold", distanceThreshold: dt)))
                .SelectMany(x => new[] { x.CloneWithValues(algorithmName: "Eigen"), x.CloneWithValues(algorithmName: "Fisher") });

            var experimentParametersCollection2 = new[]
            {
                experimentTemplate.CloneWithValues(experimentName: "MUCT", algorithmName: "Eigen"),
                experimentTemplate.CloneWithValues(experimentName: "MUCT", algorithmName: "Fisher"),
            };

            foreach (var parameters in experimentParametersCollection)
            {
                var homeDirectory = Directory.GetCurrentDirectory() + $"\\..\\..\\..\\";

                var resultDirectory = homeDirectory + "Experiment" +
                                                      $"_{parameters.ExperimentName}" +
                                                      $"_{parameters.AlgorithmName}" +
                                                      $"_{parameters.ScaleFactor}" +
                                                      $"_{parameters.MinNeigbours}" +
                                                      $"_{parameters.NumComponents}" +
                                                      $"_{parameters.DistanceThreshold}\\";

                Directory.CreateDirectory(resultDirectory);

                var faceDetectionService =
                    new HaarClassifierFaceDetectionService(parameters.ScaleFactor, parameters.MinNeigbours, new HaarClassifierFaceSize(50, 50));

                var faceRecognitionService = parameters.AlgorithmName == "Eigen"
                    ? new EigenFaceRecognitionService(faceDetectionService, parameters.NumComponents, parameters.DistanceThreshold)
                    : new FisherFaceRecognitionService(faceDetectionService, parameters.NumComponents, parameters.DistanceThreshold)
                    as IFaceRecognitionService;

                var datasetSplittingService = new DatasetSplittingService();
                var datasetLabelingService = new DatasetLabelingService();
                var lookupSerializerService = new LookupJsonSerializerService<int, string>();

#if TRAINING
                var dataset = datasetSplittingService.SplitDataset(homeDirectory + "Speculo.Resources\\Datasets\\yalefaces", 0.2f);
                var trainingSet = datasetLabelingService.LabelDataset(dataset[DatasetEntryType.Training]);
                var testSet = datasetLabelingService.LabelDataset(dataset[DatasetEntryType.Test]);
                File.WriteAllText(resultDirectory + "testset.json", lookupSerializerService.Serialize(testSet));

                faceRecognitionService.TrainFromImages(trainingSet);
                faceRecognitionService.SaveToXml(resultDirectory + "model.yml");
#else
                var testSet = File.ReadAllText(resultDirectory + "testset.json").DeserializeLookup<int, string>();

                faceRecognitionService.TrainFromXml(resultDirectory + "model.yml");
#endif

                var falseAcceptanceCounter = Enumerable
                    .Range(0, 1000)
                    .ToDictionary(x => x, _ => 0);

                var analysisEntries = testSet
                    .Select(x =>
                    {
                        var successCount = 0;

                        foreach (var line in x)
                        {
                            var recognition = faceRecognitionService.RecognizeFace(line);
                            if (recognition == null || recognition.Label < 0) continue;

                            if (recognition.Label == x.Key)
                            {
                                successCount++;
                            }
                            else
                            {
                                falseAcceptanceCounter[recognition.Label]++;
                            }
                        }

                        return new AnalysisEntry
                        {
                            Label = x.Key,
                            AcceptCount = successCount,
                            TotalCount = x.Count(),
                            Accuracy = successCount * 1.0f / x.Count(),
                            FalseRejectionRate = (x.Count() - successCount) * 1.0f / x.Count()
                        };
                    })
                    .Select(x =>
                    {
                        var falseAcceptances = falseAcceptanceCounter.ContainsKey(x.Label) ? falseAcceptanceCounter[x.Label] : 0;
                        var falseAttempts = testSet.Sum(g => g.Count()) - x.TotalCount;
                        x.FalseAcceptanceRate = falseAcceptances * 1.0f / falseAttempts;
                        return x;
                    })
                    .OrderByDescending(x => x.Accuracy).ThenByDescending(x => x.TotalCount)
                    .ToList();

                var totalCorrectlyAccepted = analysisEntries.Sum(x => x.AcceptCount);
                var totalIncorrectlyAccepted = falseAcceptanceCounter.Sum(x => x.Value);
                var total = analysisEntries.Sum(x => x.TotalCount);

                var analysisContainer = new AnalysisContainer
                {
                    TotalFalseAcceptanceRate = totalIncorrectlyAccepted * 1.0f / total,
                    TotalFalseRejectionRate = (total - totalCorrectlyAccepted) * 1.0f / total,
                    Parameters = parameters,
                    Entries = analysisEntries
                };

                File.WriteAllText(resultDirectory + "results.json", JsonConvert.SerializeObject(analysisContainer));

                Logger.Info($"Processing done, results saved in {Path.GetDirectoryName(resultDirectory)}.");
            }
        }
    }
}
