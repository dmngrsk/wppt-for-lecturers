﻿namespace Speculo.Experiment
{
    public class ExperimentParameters
    {
        public string ExperimentName { get; set; }
        public string AlgorithmName { get; set; }
        public float ScaleFactor { get; set; }
        public int MinNeigbours { get; set; }
        public int NumComponents { get; set; }
        public float DistanceThreshold { get; set; }

        public ExperimentParameters CloneWithValues(
            string experimentName = null,
            string algorithmName = null,
            float? scaleFactor = null,
            int? minNeighbours = null,
            int? numComponents = null,
            float? distanceThreshold = null)
        {
            var cloned = (ExperimentParameters)MemberwiseClone();

            cloned.ExperimentName = experimentName ?? cloned.ExperimentName;
            cloned.AlgorithmName = algorithmName ?? cloned.AlgorithmName;
            cloned.ScaleFactor = scaleFactor ?? cloned.ScaleFactor;
            cloned.MinNeigbours = minNeighbours ?? cloned.MinNeigbours;
            cloned.NumComponents = numComponents ?? cloned.NumComponents;
            cloned.DistanceThreshold = distanceThreshold ?? cloned.DistanceThreshold;

            return cloned;
        }
    }
}
