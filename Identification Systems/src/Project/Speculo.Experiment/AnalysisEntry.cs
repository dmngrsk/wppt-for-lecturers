﻿namespace Speculo.Experiment
{
    public class AnalysisEntry
    {
        public int Label { get; set; }
        public int AcceptCount { get; set; }
        public int TotalCount { get; set; }
        public float Accuracy { get; set; }
        public float FalseAcceptanceRate { get; set; }
        public float FalseRejectionRate { get; set; }
    }
}
