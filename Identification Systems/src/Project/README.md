# Identification Systems project

Implement chosen biometrics in form of a simple authentication/identification system. Create a toolkit for enrollment of raw biometric data and processing them into the biometric templates. Perform simulations and evaluate FAR/FRR curves, try different sensitivity thresholds, think about template model. Feel free to use libraries like [OpenCV](https://opencv.org/) or [OpenBR](http://openbiometrics.org/), the main aim of the project is to perform dozen experiments and dive into practical aspects of using biometrics.

- Option 1 - Keystrokes/Touchscreen Dynamics
- Option 2 - **Face Recognition (chosen)**
- Option 3 - Iris Recognition
- Option 4 - Fingerprint
- Option 5 - Signature/Handwriting Recognition