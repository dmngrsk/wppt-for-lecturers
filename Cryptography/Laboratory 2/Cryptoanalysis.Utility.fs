module Cryptoanalysis.Utility

let getKeyByte c k = if (k = -1) then ('#' |> int) else (c ^^^ k)

let foldKey (ks:seq<int>) = Seq.fold (fun acc k -> acc + (k |> string) + " ") "" ks

let foldChars (ms:seq<int>) = Seq.fold (fun acc m -> acc + (m |> char |> string)) "" ms