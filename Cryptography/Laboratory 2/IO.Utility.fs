module IO.Utility

open System
open System.IO

let loadCipherTexts file = 
    File.ReadAllLines("input.bin") |> Seq.ofArray 
    |> Seq.filter (fun s -> s.StartsWith("0") || s.StartsWith("1"))
    |> Seq.map (fun s -> (s.Split ' '))
    |> Seq.map (fun ss -> (Seq.map (fun n -> Convert.ToInt32(n, 2)) ss))