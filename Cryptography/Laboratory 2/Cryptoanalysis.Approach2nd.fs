module Cryptoanalysis.Approach2nd

open Cryptoanalysis.Utility

type private CharacterCandidate = int * int

let private alphabet_space = ' '
let private alphabet =
    [| alphabet_space; '.'; ','; '!'; '?'; ':'; ';'; '\''; '"'; '-'; '('; ')'; '*' |] |> Seq.map (fun c -> c |> int)
    |> Seq.append (System.Linq.Enumerable.Range(65, 26) |> Seq.cast<int>)
    |> Seq.append (System.Linq.Enumerable.Range(97, 26) |> Seq.cast<int>)
    |> Seq.toArray

let rec private findByteInCandidates candidates c =
    match candidates with
    | (byte, _)::cdts ->
        let check = byte ^^^ c
        match (Array.contains check alphabet) with
        | true -> byte
        | false -> findByteInCandidates cdts c
    | [] -> -1

let private findKeyByte i css m =
    let scores = Array.create 256 0
    for cs in css do
        if i < (Seq.length cs) then
            for byte in 0..255 do
                let check = byte ^^^ (Seq.item i cs)
                if (Array.contains check alphabet) then scores.[byte] <- scores.[byte] + 1
                if (check = (alphabet_space |> int)) then scores.[byte] <- scores.[byte] + 1

    let candidates = scores |> Seq.mapi (fun b sc -> (b, sc)) |> Seq.sortBy (fun (b, sc) -> -sc) |> Seq.toList
    findByteInCandidates candidates m

let private findKey css id =
    let requested_cs = Seq.item id css
    let key_length = Seq.length requested_cs
    let key = Array.create key_length -1

    for i in 0..(key_length - 1) do
        key.[i] <- findKeyByte i css (Seq.item i requested_cs)
  
    key

let findMessage2 css i = 
    let cs = Seq.item i css
    let ks = findKey css i

    Seq.map2 getKeyByte cs ks |> foldChars