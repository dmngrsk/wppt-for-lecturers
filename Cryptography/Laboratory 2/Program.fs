﻿open System
open Cryptoanalysis.Approach1st
open Cryptoanalysis.Approach2nd
open IO.Utility

[<EntryPoint>]
let main argv =
    let css = loadCipherTexts "input.bin"
    let id = if (argv.Length > 0) then (argv.[0] |> int) else (Seq.length css) - 1

    let msg1 = findMessage1 css id
    printfn "Approach #1 message:\n%s\n" msg1

    let msg2 = findMessage2 css id
    printfn "Approach #2 message:\n%s" msg2

    0