module Cryptoanalysis.Approach1st

open Cryptoanalysis.Utility

type private SpaceCandidate = int * int * int

let private getSpaceCandidates css =
    let space_counter = css |> Seq.map (fun s -> Array.create (s |> Seq.length) 0) |> Seq.toArray
    
    css |> Seq.iteri (fun i1 cs1 -> 
        css |> Seq.iteri (fun i2 cs2 -> 
            if i1 <> i2 then
                let min = min (Seq.length cs1) (Seq.length cs2)
                for ii in 0..(min - 1) do
                    if (Seq.item ii cs1) ^^^ (Seq.item ii cs2) &&& 0xC0 > 0 then
                        space_counter.[i1].[ii] <- space_counter.[i1].[ii] + 1
                        space_counter.[i2].[ii] <- space_counter.[i2].[ii] + 1))
    
    space_counter
    |> Seq.mapi (fun i cs -> cs |> Seq.mapi (fun ii c -> (i, ii, c)))
    |> Seq.collect id

let private findKey css = 
    let key = Array.create (css |> Seq.map (fun s -> Seq.length s) |> Seq.max) -1

    (getSpaceCandidates css)
    |> Seq.sortBy (fun (_, _, c) -> -c)
    |> Seq.iter (fun (i, ii, _) -> if key.[ii] = -1 then key.[ii] <- 0x20 ^^^ (css |> Seq.item i |> Seq.item ii))
    
    key

let findMessage1 css i = 
    let cs = Seq.item i css
    let ks = findKey css

    Seq.map2 getKeyByte cs ks |> foldChars