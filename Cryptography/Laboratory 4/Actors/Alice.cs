using System;
using MerklePuzzle.Extensions;
using MerklePuzzle.Models;
using MerklePuzzle.Oracles;
using MerklePuzzle.Utilities;

namespace MerklePuzzle.Actors
{
    public class Alice
    {
        private const int L = 1;

        private readonly RandomOracle _randomOracle;
        private readonly EncryptionOracle _encryptionOracle;
        private readonly CommunicationChannel _communicationChannel;

        private byte[] _k1, _k2, _key;
        
        public Alice(CommunicationChannel communicationChannel)
        {
            _randomOracle = new RandomOracle();
            _encryptionOracle = new EncryptionOracle();
            _communicationChannel = communicationChannel;
        }

        public void PreparePuzzles()
        {
            _k1 = _randomOracle.Random(L).ToByteArray(16);
            _k2 = _randomOracle.Random(L).ToByteArray(16);

            _communicationChannel.Constant = _randomOracle.Random(L);
            _communicationChannel.ConstantBytes = _communicationChannel.Constant.ToByteArray(16);
            Say($"Selected constant: {_communicationChannel.Constant}.");

            for (long i = 0; i < _communicationChannel.Size; ++i)
            {
                if (i % LoggingUtilities.LoggingFrequency == 0) Say($"Building puzzle #{i}...");
                if (_communicationChannel.PuzzleId != null || _communicationChannel.PuzzleFailed) break;

                var puzzle = PreparePuzzle(i);
                _communicationChannel.Puzzles.Enqueue(puzzle);
            }

            while (_communicationChannel.PuzzleId == null && !_communicationChannel.PuzzleFailed)
            {
            }

            if (!_communicationChannel.PuzzleFailed)
            {
                _key = _encryptionOracle.Encrypt(_k2, _communicationChannel.PuzzleId);
            }
        }

        public void Say(string message)
        {
            Console.WriteLine($"Alice:\t{message}");
        }

        public byte[] GetKey()
        {
            return _key;
        }

        private Puzzle PreparePuzzle(long i)
        {
            var iBytes = BitConverter.GetBytes(i);

            var puzzleId = _encryptionOracle.Encrypt(_k1, iBytes);
            var puzzleKey = _encryptionOracle.Encrypt(_k2, puzzleId);

            var randomKey = _randomOracle
                .Random(_communicationChannel.Constant * _communicationChannel.Size)
                .ToByteArray(16);

            return new Puzzle(_encryptionOracle.Encrypt(randomKey, puzzleId, puzzleKey, _communicationChannel.ConstantBytes));
        }
    }
}