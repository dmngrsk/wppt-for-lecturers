using System;
using System.Linq;
using MerklePuzzle.Extensions;
using MerklePuzzle.Models;
using MerklePuzzle.Oracles;
using MerklePuzzle.Utilities;

namespace MerklePuzzle.Actors
{
    public class Bob
    {
        private readonly RandomOracle _randomOracle;
        private readonly EncryptionOracle _encryptionOracle;
        private readonly CommunicationChannel _communicationChannel;

        private byte[] _key;

        public Bob(CommunicationChannel communicationChannel)
        {
            _randomOracle = new RandomOracle();
            _encryptionOracle = new EncryptionOracle();
            _communicationChannel = communicationChannel;
        }

        public void SolveSelectedPuzzle()
        {
            var selectedPuzzle = GetPuzzle();

            SolvePuzzle(selectedPuzzle);
        }

        public void Say(string message)
        {
            Console.WriteLine($"Bob:\t{message}");
        }

        public byte[] GetKey()
        {
            return _key;
        }

        private Puzzle GetPuzzle()
        {
            var puzzleIndex = _randomOracle.Random(_communicationChannel.Size) % 1000;
            Say($"Waiting for puzzle #{puzzleIndex}...");

            Puzzle puzzle = null;

            for (var i = 0; i < puzzleIndex; ++i)
            {
                while (!_communicationChannel.Puzzles.TryDequeue(out puzzle))
                {
                }
            }

            return puzzle;
        }

        private void SolvePuzzle(Puzzle puzzle)
        {
            if (puzzle == null) return;
            var limit = _communicationChannel.Constant * _communicationChannel.Size;

            for (long i = 0; i < limit; ++i)
            {
                if (i % LoggingUtilities.LoggingFrequency == 0) Say($"Trying to solve selected puzzle, attempt #{i}/{limit}...");
                _communicationChannel.Puzzles.Clear();

                var randomKey = i.ToByteArray(16);
                var decrypted = _encryptionOracle.Decrypt(randomKey, puzzle.Content);

                var id = decrypted.Take(32).ToArray();
                var key = decrypted.Skip(32).Take(48).ToArray();
                var tempConstant = decrypted.Skip(80).ToArray();

                if (tempConstant.SequenceEqual(_communicationChannel.ConstantBytes))
                {
                    _key = key;
                    _communicationChannel.PuzzleId = id;

                    Say($"Found the key in attempt #{i}.");
                    return;
                }
            }

            _communicationChannel.PuzzleFailed = true;
        }
    }
}