﻿using System.Text;

namespace MerklePuzzle.Extensions
{
    public static class ByteArrayExtensions
    {
        public static string ToHexadecimalString(this byte[] ba)
        {
            if (ba == null) return null;

            var hex = new StringBuilder(ba.Length * 2);
            foreach (var b in ba) hex.AppendFormat("{0:x2}", b);

            return hex.ToString();
        }
    }
}
