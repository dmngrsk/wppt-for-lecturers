﻿using System;
using System.Linq;

namespace MerklePuzzle.Extensions
{
    public static class LongExtensions
    {
        public static byte[] ToByteArray(this long input, int size)
        {
            var bytes = BitConverter.GetBytes(input);

            return bytes.Concat(Enumerable.Repeat((byte)0, size - bytes.Length)).ToArray();
        }

        public static long AbsoluteValue(this long input)
        {
            return input > 0 ? input : -input;
        }
    }
}
