﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MerklePuzzle.Models
{
    public class Puzzle
    {
        public byte[] Content { get; }

        public Puzzle(byte[] content)
        {
            Content = content;
        }
    }
}
