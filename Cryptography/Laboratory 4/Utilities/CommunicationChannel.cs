﻿using System.Collections.Concurrent;
using MerklePuzzle.Models;

namespace MerklePuzzle.Utilities
{
    public class CommunicationChannel
    {
        public long Size { get; }
        public ConcurrentQueue<Puzzle> Puzzles { get; }
        public long Constant { get; set; }
        public byte[] ConstantBytes { get; set; }
        public byte[] PuzzleId { get; set; }
        public bool PuzzleFailed { get; set; }

        public CommunicationChannel(long size)
        {
            Size = size;
            Puzzles = new ConcurrentQueue<Puzzle>();
        }
    }
}
