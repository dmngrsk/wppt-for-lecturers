using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace MerklePuzzle.Oracles
{
    public class EncryptionOracle
    {
        public byte[] Encrypt(byte[] key, params byte[][] bytes)
        {
            var messagesConcat = bytes.SelectMany(b => b).ToArray();

            using (var aes = Aes.Create())
            {
                aes.KeySize = key.Length * 8;
                aes.Key = key;
                aes.IV = key;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.Zeros;

                var enc = aes.CreateEncryptor(aes.Key, aes.IV);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                    {
                        cs.Write(messagesConcat, 0, messagesConcat.Length);
                    }

                    return aes.IV.Concat(ms.ToArray()).ToArray();
                }
            }
        }

        public byte[] Decrypt(byte[] key, byte[] bytes)
        {
            var iv = bytes.Take(16).ToArray();
            var cipherText = bytes.Skip(16).ToArray();

            var buffer = new byte[cipherText.Length];

            using (var aes = Aes.Create())
            {
                aes.KeySize = key.Length * 8;
                aes.Key = key;
                aes.IV = key;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.Zeros;

                var dec = aes.CreateDecryptor(aes.Key, aes.IV);

                using (var ms = new MemoryStream(cipherText))
                {
                    using (var cs = new CryptoStream(ms, dec, CryptoStreamMode.Read))
                    {
                        cs.Read(buffer, 0, buffer.Length);
                    }
                }
            }

            return buffer;
        }
    }
}