using System;
using System.Security.Cryptography;
using MerklePuzzle.Extensions;

namespace MerklePuzzle.Oracles
{
    public class RandomOracle
    {
        public long Random(long size)
        {
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[8];
                rngCsp.GetBytes(bytes);

                return BitConverter.ToInt64(bytes, 0).AbsoluteValue() % size + 1;
            }
        }
    }
}