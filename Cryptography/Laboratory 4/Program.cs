﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MerklePuzzle.Actors;
using MerklePuzzle.Extensions;
using MerklePuzzle.Utilities;

namespace MerklePuzzle
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var arg = args.Length > 0 ? int.Parse(args[0]) : 24;
            var size = Convert.ToInt64(Math.Pow(2, arg));

            var channel = new CommunicationChannel(size);

            var alice = new Alice(channel);
            var bob = new Bob(channel);

            var sw = new Stopwatch();
            sw.Start();

            Parallel.Invoke(
                () => alice.PreparePuzzles(),
                () => bob.SolveSelectedPuzzle());
            
            sw.Stop();
            Console.WriteLine($"Finished in: {sw.Elapsed}");

            if (channel.PuzzleFailed)
            {
                Console.WriteLine("MerklePuzzle failed: Bob could not find the secret key.");
            }
            else
            {
                Console.WriteLine("Alice's key:");
                Console.WriteLine(alice.GetKey().ToHexadecimalString());
                Console.WriteLine("Bob's key:");
                Console.WriteLine(bob.GetKey().ToHexadecimalString());
            }

#if DEBUG
            Console.ReadKey();
#endif
        }
    }
}
