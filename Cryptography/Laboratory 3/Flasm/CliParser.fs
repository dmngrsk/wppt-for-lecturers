﻿module Flasm.CliParser

open Argu
open Flasm.Core.EnumTypes

type EncryptorCliArguments =
    | [<ExactlyOnce; AltCommandLine("-m")>] Mode of EncryptionMode
    | [<ExactlyOnce; AltCommandLine("-s")>] Keystore of keystore:string
    | [<ExactlyOnce; AltCommandLine("-a")>] Alias of alias:string
    | [<ExactlyOnce; AltCommandLine("-p")>] Password of password:string
    | [<ExactlyOnce; Last; AltCommandLine("-f")>] Files of files:string list
    | Insecure
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Files _ -> "a list of paths to files to encrypt. Decryptable files need to be saved with an .aes extension, while encryptable files will be saved under it."
            | Mode _ -> "block cipher algorithm used for encryption."
            | Keystore _ -> "path to the keystore."
            | Alias _ -> "alias of a key in the keystore."
            | Password _ -> "password to the keystore."
            | Insecure _ -> "allow for insecure encryption with an incremental initialization vector."
and ChallengerCliArguments =
    | [<ExactlyOnce; AltCommandLine("-m")>] Mode of EncryptionMode
    | [<ExactlyOnce; AltCommandLine("-s")>] Keystore of keystore:string
    | [<ExactlyOnce; AltCommandLine("-a")>] Alias of alias:string
    | [<ExactlyOnce; AltCommandLine("-p")>] Password of password:string
    | [<ExactlyOnce; AltCommandLine("-i")>] Input of in1:string * in2:string
    | [<ExactlyOnce; AltCommandLine("-o")>] Output of out:string
    | [<ExactlyOnce>] Insecure
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Input _ -> "two paths to input files containing challenge plaintexts."
            | Output _ -> "path to save an output file containing challenge ciphertext."
            | Mode _ -> "block cipher algorithm used for encryption."
            | Keystore _ -> "path to the keystore."
            | Alias _ -> "alias of a key in the keystore."
            | Password _ -> "password to the keystore."
            | Insecure _ -> "allow for insecure encryption with an incremental initialization vector."
and DistinguisherCliArguments =
    | [<ExactlyOnce; AltCommandLine("-m")>] Mode of EncryptionMode
    | [<ExactlyOnce; AltCommandLine("-s")>] Keystore of keystore:string
    | [<ExactlyOnce; AltCommandLine("-a")>] Alias of alias:string
    | [<ExactlyOnce; AltCommandLine("-p")>] Password of password:string
    | [<ExactlyOnce; AltCommandLine("-i")>] Iterations of iterations:int
    | [<ExactlyOnce>] Insecure
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Mode _ -> "block cipher algorithm used for encryption."
            | Keystore _ -> "path to a JCEKS keystore."
            | Alias _ -> "alias of a key in the keystore."
            | Password _ -> "password to the keystore."
            | Iterations _ -> "amount of experiment iterations to perform."
            | Insecure _ -> "allow for insecure encryption with an incremental initialization vector."
and CliCommands =
    | [<CliPrefix(CliPrefix.None)>] Encrypt of ParseResults<EncryptorCliArguments>
    | [<CliPrefix(CliPrefix.None)>] Decrypt of ParseResults<EncryptorCliArguments>
    | [<CliPrefix(CliPrefix.None)>] Challenge of ParseResults<ChallengerCliArguments>
    | [<CliPrefix(CliPrefix.None)>] Distinguish of ParseResults<DistinguisherCliArguments>
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Encrypt _ -> "encrypts a given sequence files."
            | Decrypt _ -> "decrypts a given sequence files."
            | Challenge _ -> "selects one of the two given files at random, encrypts it and saves to output file."
            | Distinguish _ -> "performs an experiment over given encryption mode."

let parser = ArgumentParser.Create<CliCommands>(programName = "flasm")