﻿module Flasm.Program

open Argu
open Flasm.Core.Startup
open Flasm.CliParser
open Flasm.Runners

[<EntryPoint>]
let main argv =
    startup ()
    
    try
        let results = parser.ParseCommandLine argv

        match results.GetSubCommand() with
        | Encrypt _ ->
            let command = results.GetResult Encrypt
            let mode = command.GetResult EncryptorCliArguments.Mode
            let keystore = command.GetResult EncryptorCliArguments.Keystore
            let alias = command.GetResult EncryptorCliArguments.Alias
            let password = command.GetResult EncryptorCliArguments.Password
            let insecure = command.Contains EncryptorCliArguments.Insecure
            let files = command.GetResult EncryptorCliArguments.Files
            encrypt mode keystore alias password insecure files

        | Decrypt _ ->
            let command = results.GetResult Decrypt
            let mode = command.GetResult EncryptorCliArguments.Mode
            let keystore = command.GetResult EncryptorCliArguments.Keystore
            let alias = command.GetResult EncryptorCliArguments.Alias
            let password = command.GetResult EncryptorCliArguments.Password
            let insecure = command.Contains EncryptorCliArguments.Insecure
            let files = command.GetResult EncryptorCliArguments.Files
            decrypt mode keystore alias password insecure files

        | Challenge _ ->
            let command = results.GetResult Challenge
            let mode = command.GetResult ChallengerCliArguments.Mode
            let keystore = command.GetResult ChallengerCliArguments.Keystore
            let alias = command.GetResult ChallengerCliArguments.Alias
            let password = command.GetResult ChallengerCliArguments.Password
            let insecure = command.Contains ChallengerCliArguments.Insecure
            let input = command.GetResult ChallengerCliArguments.Input
            let output = command.GetResult ChallengerCliArguments.Output
            challenge mode keystore alias password insecure input output

        | Distinguish _ ->
            let command = results.GetResult Distinguish
            let mode = command.GetResult DistinguisherCliArguments.Mode
            let keystore = command.GetResult DistinguisherCliArguments.Keystore
            let alias = command.GetResult DistinguisherCliArguments.Alias
            let password = command.GetResult DistinguisherCliArguments.Password
            let iters = command.GetResult DistinguisherCliArguments.Iterations
            let insecure = command.Contains DistinguisherCliArguments.Insecure
            distinguish mode keystore alias password iters insecure

    with :? ArguParseException as e -> printfn "%s" e.Message
    
    0