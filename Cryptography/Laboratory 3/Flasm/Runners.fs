module Flasm.Runners

open Flasm.Core.Encryptors
open Flasm.Core.Decryptors
open Flasm.Core.Challengers
open Flasm.Core.Distinguishers
open Flasm.Core.InitVectors
open Flasm.Core.KeyStore
open System
open System.IO

let private readInt msg = printfn "%s" msg; Console.ReadLine() |> int
let private readString msg = printfn "%s" msg; Console.ReadLine()
let private loadBytes path = File.ReadAllBytes(path)
let private saveBytes bs path = File.WriteAllBytes(path, bs)
let private appendAesExtension (path:string) = path + ".aes"
let private trimAesExtension (path:string) = path.Substring(0, path.Length - 4)

let encrypt mode keystore alias password insecure files =
    let key = getKey keystore alias password
    let ivg = getInitVectorGenerator insecure
    let enc = getEncryptor mode key ivg

    files |> Seq.iter (fun p -> (saveBytes (enc (loadBytes p)) (appendAesExtension p)))

let decrypt mode keystore alias password insecure files =
    let key = getKey keystore alias password
    let dec = getDecryptor mode key

    files |> Seq.iter (fun p -> (saveBytes (dec (loadBytes p)) (trimAesExtension p)))

let challenge mode keystore alias password insecure (in0, in1) output =
    let key = getKey keystore alias password
    let ivg = getInitVectorGenerator insecure
    let enc = getEncryptor mode key ivg
    let chal = getChallengerNoBit enc

    saveBytes (chal (loadBytes in0) (loadBytes in1)) output

let distinguish mode keystore alias password iters insecure =
    let key = getKey keystore alias password
    let ivg = getInitVectorGenerator insecure
    let enc = getEncryptor mode key ivg
    let chal = getChallenger enc
    let d = getDistinguisher mode

    let res = Seq.reduce (+) (Seq.map (fun _ -> (d enc chal)) (Array.create iters 0))
    printfn "Distinguisher managed to guess correctly %d times out of %d iterations (%.2f%% success rate)." res iters ((float res) / (float iters) * 100.0)