module Flasm.Core.InitVectors

open Flasm.Core.EnumTypes
open Flasm.Core.BinaryUtils
open System
open System.Security.Cryptography

let private getRandomIV () =
    use aes = Aes.Create()
    aes.GenerateIV()
    aes.IV

let private getIncrementedIV () =
    let name = "FLASM_INCREMENTABLE_IV"
    let var = Environment.GetEnvironmentVariable(name)

    match var with
    | null ->
        let iv = getRandomIV ()
        Environment.SetEnvironmentVariable(name, (bytesToHex iv))
        iv
    | vec -> 
        let incr = incrementHex vec
        Environment.SetEnvironmentVariable(name, incr)
        hexToBytes incr

let getInitVectorGenerator insecure =
    match insecure with
    | true -> getIncrementedIV
    | false -> getRandomIV