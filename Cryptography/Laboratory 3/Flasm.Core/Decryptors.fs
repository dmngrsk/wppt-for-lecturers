module Flasm.Core.Decryptors

open Flasm.Core.EnumTypes
open System
open System.IO
open System.Security.Cryptography

let private decryptChilkatBase key mode bs =
    let aes = new Chilkat.Crypt2()
    let unlocked = aes.UnlockComponent("trial_key")
    if not unlocked then failwith "ERROR: could not launch encryption cryptoservice."

    aes.CryptAlgorithm <- "aes"
    aes.KeyLength <- (Array.length key) * 8
    aes.CipherMode <- mode

    aes.SecretKey <- key
    aes.IV <- Array.take 16 bs

    aes.DecryptBytes (Array.skip (aes.IV.Length) bs)
    
let private decryptDotnetBase key mode bs =
    use aes = Aes.Create()
    aes.KeySize <- (Array.length key) * 8
    aes.Mode <- mode

    aes.Key <- key
    aes.IV <- Array.take 16 bs

    let buffer = Array.create (bs.Length - aes.IV.Length) (byte 0)
    let dec = aes.CreateDecryptor(aes.Key, aes.IV)
    use ms = new MemoryStream(Array.skip (aes.IV.Length) bs)
    use cs = new CryptoStream(ms, dec, CryptoStreamMode.Read)
    cs.Read(buffer, 0, buffer.Length) |> ignore

    buffer
    
let private decryptCBC key bs = decryptChilkatBase key "cbc" bs
let private decryptCFB key bs = decryptChilkatBase key "cfb" bs
let private decryptCTR key bs = decryptChilkatBase key "ctr" bs
let private decryptECB key bs = decryptChilkatBase key "ecb" bs
let private decryptOFB key bs = decryptChilkatBase key "ofb" bs
// let private decryptCBC key bs = decryptDotnetBase key CipherMode.CBC bs
// let private decryptECB key bs = decryptDotnetBase key CipherMode.ECB bs

let getDecryptor mode key =
    match mode with
    | EncryptionMode.CBC -> decryptCBC key
    | EncryptionMode.CFB -> decryptCFB key
    | EncryptionMode.CTR -> decryptCTR key
    | EncryptionMode.ECB -> decryptECB key
    | EncryptionMode.OFB -> decryptOFB key
    | _ -> failwith "ERROR: something went horribly wrong."