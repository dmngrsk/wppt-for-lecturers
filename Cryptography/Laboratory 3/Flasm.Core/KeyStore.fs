module Flasm.Core.KeyStore

open Chilkat
open Flasm.Core.BinaryUtils
    
let rec private findSecretKey (jks:JavaKeyStore) (alias:string) (password:string) (i:int) =
    if i < jks.NumSecretKeys then
        match jks.GetSecretKeyAlias(i) with
        | a when a = alias -> hexToBytes (jks.GetSecretKey(password, i, "hex"))
        | _ -> findSecretKey jks alias password (i + 1)
    else
        failwith "ERROR: key not found in the keystore."

let private fail (msg:string) =
    if msg.Contains("No such file") then failwith ("ERROR: invalid keystore path.")
    if msg.Contains("verification failed") then failwith ("ERROR: invalid keystore password.")
    if msg.Contains("Invalid JKS magic number") then failwith ("ERROR: invalid keystore format, expected JCEKS.")
    failwith "ERROR: something went horribly wrong."

let getKey store alias password =
    let jks = new JavaKeyStore()
    let loaded = jks.LoadFile(password, store)
    if not loaded then fail jks.LastErrorText

    findSecretKey jks alias password 0