module Flasm.Core.Distinguishers

open Flasm.Core.BinaryUtils
open Flasm.Core.EnumTypes
open System

let private randomBytes s =
    let rng = new Random()
    Array.init s (fun _ -> (byte (rng.Next(0, 256))))

let rec private randomBytesNotEqual s bs =
    let rnd = randomBytes s
    match rnd with
    | rnd when rnd = bs -> (randomBytesNotEqual s bs)
    | _ -> rnd

let private distinguishCBC enc chal =
    let mq1 = randomBytes 16
    let mq2 = randomBytes 16
    let mq = Array.append mq1 mq2
    let cq = enc mq
    let ivq = Array.take 16 cq

    let ivx = ivq |> bytesToHex |> incrementHex |> hexPadZeroes 32 |> hexToBytes
    let m01 = Array.map2 (^^^) (Array.map2 (^^^) mq1 ivq) ivx
    let m11 = randomBytesNotEqual 16 m01
    let mx2 = randomBytes 16

    let m0 = Array.append m01 mx2
    let m1 = Array.append m11 mx2
    let (cx, b) = chal m0 m1

    let cq1 = Array.take 16 (Array.skip 16 cq)
    let cx1 = Array.take 16 (Array.skip 16 cx)
    
    let x = if cq1 = cx1 then 0 else 1
    if x <> b then 0 else 1

let getDistinguisher mode = 
    match mode with
    | EncryptionMode.CBC -> distinguishCBC
    | _ -> failwith "ERROR: distinguisher for this encryption mode is not implemented."