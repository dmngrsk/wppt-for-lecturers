module Flasm.Core.Startup

let private unlockChilkat () =
    let glob = new Chilkat.Global()
    let unlocked = glob.UnlockBundle("trial_key")
    if not unlocked then failwith "ERROR: could not launch keystore cryptoservice."

let startup () =
    unlockChilkat ()