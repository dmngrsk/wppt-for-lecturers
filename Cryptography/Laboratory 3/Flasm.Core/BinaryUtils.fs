module Flasm.Core.BinaryUtils

open System
open System.Globalization
open System.Numerics
open System.Text

let bytesToHex bs = 
    BitConverter.ToString(bs).Replace("-", "")

let hexToBytes hex =
    hex 
    |> Seq.windowed 2
    |> Seq.mapi (fun i j -> (i, j))
    |> Seq.filter (fun (i, j) -> i % 2 = 0)
    |> Seq.map (fun (_, j) -> Byte.Parse(new String(j), NumberStyles.AllowHexSpecifier))
    |> Array.ofSeq

let toAsciiBytes (msg:string) =
    Encoding.ASCII.GetBytes msg

let incrementHex hex =
    let value = BigInteger.Parse(hex, NumberStyles.AllowHexSpecifier)
    let incr = value + 1I
    incr.ToString("X")

let hexPadZeroes cnt (hex:string) =
    if cnt > hex.Length then new string('0', cnt - hex.Length) + hex else hex