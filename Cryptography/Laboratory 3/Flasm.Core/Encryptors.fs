module Flasm.Core.Encryptors

open Flasm.Core.EnumTypes
open System.IO
open System.Security.Cryptography

let private encryptChilkatBase key ivg mode bs =
    let aes = new Chilkat.Crypt2()
    let unlocked = aes.UnlockComponent("trial_key")
    if not unlocked then failwith "ERROR: could not launch encryption cryptoservice."

    aes.CryptAlgorithm <- "aes"
    aes.KeyLength <- (Array.length key) * 8
    aes.CipherMode <- mode

    aes.SecretKey <- key
    aes.IV <- ivg ()

    Array.append (aes.IV) (aes.EncryptBytes(bs))

let private encryptDotnetBase key ivg mode bs =
    use aes = Aes.Create()
    aes.KeySize <- (Array.length key) * 8
    aes.Mode <- mode

    aes.Key <- key
    aes.IV <- ivg ()

    let enc = aes.CreateEncryptor(aes.Key, aes.IV)
    use ms = new MemoryStream()
    use cs = new CryptoStream(ms, enc, CryptoStreamMode.Write)
    cs.Write(bs, 0, bs.Length)
   
    Array.append (aes.IV) (ms.ToArray())

let private encryptCBC key ivg bs = encryptChilkatBase key ivg "cbc" bs
let private encryptCFB key ivg bs = encryptChilkatBase key ivg "cfb" bs
let private encryptCTR key ivg bs = encryptChilkatBase key ivg "ctr" bs
let private encryptECB key ivg bs = encryptChilkatBase key ivg "ecb" bs
let private encryptOFB key ivg bs = encryptChilkatBase key ivg "ofb" bs
// let private encryptCBC key ivg bs = encryptDotnetBase key ivg CipherMode.CBC bs
// let private encryptECB key ivg bs = encryptDotnetBase key ivg CipherMode.ECB bs

let getEncryptor mode key ivg =
    match mode with
    | EncryptionMode.CBC -> encryptCBC key ivg
    | EncryptionMode.CFB -> encryptCFB key ivg
    | EncryptionMode.CTR -> encryptCTR key ivg
    | EncryptionMode.ECB -> encryptECB key ivg
    | EncryptionMode.OFB -> encryptOFB key ivg
    | _ -> failwith "ERROR: something went horribly wrong."