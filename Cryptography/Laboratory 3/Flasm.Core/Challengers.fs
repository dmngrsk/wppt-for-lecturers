module Flasm.Core.Challengers

open System.Security.Cryptography

let private roll () =
    let bs = Array.init 1 byte
    use rng = new RNGCryptoServiceProvider()
    rng.GetBytes(bs)
    (int bs.[0]) % 2

let getChallenger enc = (fun m0 m1 ->
    let x = roll ()
    match x with
    | 0 -> ((enc m0), 0)
    | 1 -> ((enc m1), 1)
    | _ -> failwith "ERROR: something went horribly wrong."
)

let getChallengerNoBit enc = (fun m0 m1 ->
    let x = roll ()
    match x with
    | 0 -> (enc m0)
    | 1 -> (enc m1)
    | _ -> failwith "ERROR: something went horribly wrong."
)