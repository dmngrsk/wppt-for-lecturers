module Flasm.Core.EnumTypes

type EncryptionMode = 
    | CBC = 1
    | CFB = 2
    | CTR = 3
    | ECB = 4
    | OFB = 5