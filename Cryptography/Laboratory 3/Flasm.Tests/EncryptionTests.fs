module Flasm.Tests.EncryptionTests

open Flasm.Core.Encryptors
open Flasm.Core.Decryptors
open Flasm.Core.InitVectors
open Flasm.Core.EnumTypes
open FsUnit
open Xunit

let private rng = new System.Random()
let private rand () = fun _ -> (byte (rng.Next(0, 256)))

let private data = Array.init (rng.Next(900, 1100)) (rand ())
let private key1 = Array.init 32 (rand ())
let private key2 = Array.init 32 (rand ())

[<Theory>]
[<InlineData(EncryptionMode.CBC, true)>]
[<InlineData(EncryptionMode.CBC, false)>]
[<InlineData(EncryptionMode.CFB, true)>]
[<InlineData(EncryptionMode.CFB, false)>]
[<InlineData(EncryptionMode.CTR, true)>]
[<InlineData(EncryptionMode.CTR, false)>]
[<InlineData(EncryptionMode.ECB, true)>]
[<InlineData(EncryptionMode.ECB, false)>]
[<InlineData(EncryptionMode.OFB, true)>]
[<InlineData(EncryptionMode.OFB, false)>]
let ``Encryption round-trip with same key returns the same message`` (mode, security) =
    let ivg = getInitVectorGenerator security
    let enc = getEncryptor mode key1 ivg
    let dec = getDecryptor mode key1

    let roundtrip = dec (enc data)
    roundtrip |> should equal data

[<Theory>]
[<InlineData(EncryptionMode.CBC, true)>]
[<InlineData(EncryptionMode.CBC, false)>]
[<InlineData(EncryptionMode.CFB, true)>]
[<InlineData(EncryptionMode.CFB, false)>]
[<InlineData(EncryptionMode.CTR, true)>]
[<InlineData(EncryptionMode.CTR, false)>]
[<InlineData(EncryptionMode.ECB, true)>]
[<InlineData(EncryptionMode.ECB, false)>]
[<InlineData(EncryptionMode.OFB, true)>]
[<InlineData(EncryptionMode.OFB, false)>]
let ``Encryption round-trip with different keys returns different messages`` (mode, security) =
    let ivg = getInitVectorGenerator security
    let enc = getEncryptor mode key1 ivg
    let dec = getDecryptor mode key2

    let roundtrip = dec (enc data)
    roundtrip |> should not' (equal data)