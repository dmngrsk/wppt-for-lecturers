module Flasm.Tests.BinaryUtilsTests

open Flasm.Core.BinaryUtils
open FsUnit
open Xunit

[<Fact>]
let ``bytesToHex works as expected for given array of bytes`` () =
    let hex = bytesToHex [| (byte 96); (byte 9); (byte 105) |]
    hex |> should equal "600969"

[<Fact>]
let ``hexToBytes works as expected for given hex string with even length`` () =
    let bs = hexToBytes "600969"
    bs |> should equal [| (byte 96); (byte 9); (byte 105) |]

[<Fact>]
let ``toAsciiBytes works as expected for given string`` () =
    let bs = toAsciiBytes "ABCabc123"
    bs |> should equal ([| 65; 66; 67; 97; 98; 99; 49; 50; 51|] |> Array.map (byte))

[<Fact>]
let ``incrementHex works as expected for given hex string`` () =
    let incrHex = incrementHex "0999"
    incrHex |> should equal "099A"

[<Fact>]
let ``incrementHex carries addition to next digits if required`` () =
    let incrHexCarry = incrementHex "0FFF"
    incrHexCarry |> should equal "1000"

[<Fact>]
let ``hexPadZeroes works as expected for given hex string`` () =
    let paddedHex = hexPadZeroes 16 "ABCD"
    paddedHex |> should equal "000000000000ABCD"