module LCG

let rec private egcd_full (a:bigint) (b:bigint) =
    if a = 0I then 
        (b, 0I, 1I)
    else
        let g, y, x = egcd_full (b % a) a
        (g, x - (b / a) * y, y)

let private modinv (a:bigint) (m:bigint) =
    let g, x, y = egcd_full a m
    if g = 1I then 
        (x + m) % m
    else
        failwith (sprintf "Modular inverse for %A in modulo %A does not exist." a m)

let moda (a:bigint) (b:bigint) (m:bigint) = (a + b) % m
let mods (a:bigint) (b:bigint) (m:bigint) = ((a + m) - b) % m
let modm (a:bigint) (b:bigint) (m:bigint) = (a * b) % m
let modd (a:bigint) (b:bigint) (m:bigint) = (a * (modinv b m)) % m

let egcd (a:bigint) (b:bigint) = 
    let g, _, _ = egcd_full a b
    g

let lcg (c:bigint) (a:bigint) (b:bigint) (seed:bigint) = 
    let computeStep (a:bigint) (b:bigint) (c:bigint) (x:bigint) = moda (modm a x c) b c
    let rec loop x = seq { yield x; yield! loop (computeStep a b c x) }
    loop (computeStep a b c seed)

let sieve n =
    seq {
        yield 2

        let mySieve = new System.Collections.Generic.HashSet<int>()
        for i in 3..2..n do
            let found = mySieve.Contains(i)
            if not found then yield i
            do for j in i..i..n do mySieve.Add(j) |> ignore
    } |> Seq.toList