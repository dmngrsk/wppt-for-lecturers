module Rand

let rand (seed:bigint) = 
    let memo = Array.create 34 0I

    memo.[0] <- seed    
    for i in 1..30 do 
        memo.[i] <- (16807I * memo.[i - 1]) % 2147483647I
    for i in 31..33 do
        memo.[i] <- memo.[i - 31]
    for i in 34..343 do
        memo.[i % 34] <- (memo.[(i - 31) % 34] + memo.[(i - 3) % 34]) % 4294967296I

    let mutable currentStep = 343

    let computeStep () = 
        currentStep <- currentStep + 1
        memo.[currentStep % 34] <- (memo.[(currentStep - 31) % 34] + memo.[(currentStep - 3) % 34]) % 4294967296I
        memo.[currentStep % 34] >>> 1

    let rec loop (x:bigint) = seq { yield x; yield! loop (computeStep ()) }
    loop (computeStep ())