module CipherCracks

open System
open FsUnit
open Xunit
open LCG
open Rand

let private nextRand = 
    let rand = new Random()
    fun (n:bigint) -> ((rand.Next((n |> int) - 1) + 1) |> bigint)

let private primes = sieve 1000000

let private roll_lcg () =
    let c = (Seq.item ((nextRand (((Seq.length primes) + 1) |> bigint) - 1I) |> int) primes) |> bigint
    let a, b, seed = (nextRand c), (nextRand c), (nextRand c)
    lcg c a b seed

let private roll_rand () =
    let seed = nextRand 2147483647I
    rand seed

let private loopCount = 10000


[<Fact>]
let ``Assignment 1: Linear congruencial generator can be cracked`` () =
    let mutable successCount = 0

    for i in 1..loopCount do
        try
            let xs_taken = 5 // Must be >= 5. The more items we take, the higher probability of success.
            let myLcg = roll_lcg ()

            let xs = Seq.take xs_taken myLcg // X_n = lcg_n
            let x0, x1, x2, x3 = ((Seq.item 0 xs), (Seq.item 1 xs), (Seq.item 2 xs), (Seq.item 3 xs))

            let ys = Seq.map2 (-) (Seq.skip 1 xs) (Seq.take (xs_taken - 1) xs) // Y_n = X_(n+1) - X_n

            let zs1 = (Seq.map2 (*) (Seq.take (xs_taken - 3) ys) (Seq.skip 2 ys))
            let zs2 = (Seq.map (fun i -> i * i) (ys |> Seq.skip 1 |> Seq.take (xs_taken - 3)))
            let zs = Seq.map (abs) (Seq.map2 (-) zs1 zs2) // Z_n = Y_n*Y_(n+2) - (Y_(n+1))^2
            
            // \forall n Z_n \equiv_{mod c} 0, so we can compute c with high probability.
            //
            // Then, we solve the following congruence in order to retrieve a and b:
            //
            // { a*X_0 + b \equiv_{mod c} X_1
            // { a*X_1 + b \equiv_{mod c} X_2
            //
            // { a \equiv_{mod c} (X_1 - X_2) / (X_0 - X_1)
            // { b \equiv_{mod c} X_2 - a*X_1
            
            let c = Seq.reduce egcd zs 
            let a = modd (mods x1 x2 c) (mods x0 x1 c) c
            let b = mods x2 (modm a x1 c) c

            let mutable last = Seq.last xs
            let mutable next_xs = myLcg |> Seq.skip xs_taken |> Seq.take 100

            for next in next_xs do
                (moda (modm a last c) b c) |> should equal next
                last <- next

            // printfn "[#%d]: Cracked the LCG with a: %A, b: %A, c: %A." i a b c
            successCount <- successCount + 1

        with e -> ()
            // printfn "[#%d]: Failed to crack the LCG. :(" i

    printfn "[ASSIGNMENT 1] Success rate: %d/%d (%.2f%%)" successCount loopCount ((successCount |> float) / (loopCount |> float) * 100.0)
    successCount |> should be (greaterThan (loopCount * 25 / 100))


[<Fact>]
let ``Assignment 2: GLIBC's rand() function can be cracked`` () =
    let mutable successCount = 0

    for i in 1..loopCount do
        try
            let myRand = roll_rand ()
            let rs = Seq.take 32 myRand |> Array.ofSeq

            let out0 = Seq.item 0 rs
            let out28 = Seq.item 28 rs

            // Taken straight from the implementation. The probability of success is 75%. 
            //
            // We've got no information about shifted bits in the output, and two summed r_xs
            // could have been odd, thus with 25% probability we will be off in our prediction by 1.
            let next_out = (out0 + out28) % 2147483648I
            next_out |> should equal (Seq.item 31 rs)
            
            // printfn "[#%d]: Cracked rand(): o_(i-31) + o_(i-3) = o_i. (%A + %A = %A.)" i out0 out28 next_out
            successCount <- successCount + 1
        with e -> ()
            // printfn "[#%d]: Failed to crack rand(), because r_(i+344-31) and r_(i+344-3) were both odd. :(" i

    printfn "[ASSIGNMENT 2] Success rate: %d/%d (%.2f%%)" successCount loopCount ((successCount |> float) / (loopCount |> float) * 100.0)
    successCount |> should be (greaterThan (loopCount * 25 / 100))