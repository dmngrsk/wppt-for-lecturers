# Embedded Security Systems, report #5: Sensor networks.

**Author**: Damian Górski.

**Date**: June 18, 2018.

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Embedded%20Systems%20Security/Report%205). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## Preliminaries about sensor networks

Sensor networks are created by a group of constrained computers (nodes), that are scattered around some area. The characteristics of such nodes are:

- low-energy consumpsion,
- constrained memory,
- constrained CPU,
- are replacable in the network (if one node fails, then the whole network does not break.

Even though the network is built of computers that could be as little as a grain of sand, it is still important to establish security in the communication between such nodes. However, we cannot use anything that's computationally expensive, like the public-key infrastructure, because securing the connection would cost almost all of the resources used by the nodes. People have been working on various protocols to enforce security with as little power used as possible, and today we will talk about one of them, which is *A Key-Management Scheme for Distributed Sensor Networks*, proposed by Laurent Eschenauer and Virgil D. Gligor.

\pagebreak

## A Key-Management Scheme for Distributed Sensor Networks

Long story short, the scheme works as follows:

1. A key pool of size `P` is established.
2. Each node draws `k` unique keys from the pool (without replacement) and loads them into the memory.
3. The nodes are deployed in the working area.
4. The nodes broadcast their keyring, in order to establish a link between nodes that share a mutual key in their keyrings.
5. The connection relation is transitive: if `a` can communicate with `b`, and `b` can communicate with `c`, then `a` can communicate with `c`. How? Something called a path-key is established, and then the connection passed through by `b`.

We will not dig into the details of how the communication works, because this is not the topic for today. Instead, we will focus on the coverage of the network after the key ring establishment (sections 1-4).

## Network coverage

It is desired that the network coverage is 100%, that is every node in the network can communicate with each other. This is not possible in all environments, though. The goal of this assignment was to create a simulation in which the nodes are assigned with some keys from the mutual keypool (sections 1, 2), distributed on a grid (section 3) and they establish a connection with other nodes (section 4). After that, we run a depth-first search on some of the nodes, and check if the amount of nodes that are reachable by that node is equal to the total amount of notes in the network.

I have prepared a .NET application that establishes the network, distributes the key and performs the search, and then exports the results into a [Graphviz](http://www.graphviz.org/about/) file, which is then rendered into a `.pdf` file with a simple Python script. I include an example of a generated image as an appendix to this document.

Generally saying, we could think of the simulation as a function with signature `float EschenauerGligor(int N, int X, int P, int k, int r)`:

- `N` specifies the amount of nodes in the network,
- `X` specifies the size of the grid (a X-by-X grid is generated),
- `P` specifies the of the mutual key pool,
- `k` specifies the amount of keys for distribution to every node,
- `r` specifies the Euclidean range in which two nodes can communicate between each other,
- The result is the percentage of nodes that a random node can reach. If this is different that 1.0, then the graph is not complete. 

## Parameter analysis

- The amount of nodes `N` in the network of size `X` that can communicate with each other is mainly constrained by the amount of keys `p` and the distance `r`.
- The more keys all nodes possess, the higher probability (note the birthday paradox) that they can communicate with each other, providing they are in range.
- Obviously, if the range increases, then the coverage will be even higher. Assuming it is not possible to change the transmission distance, then we can just add more nodes to the network, so communication with a proxy (section 5) would be possible.
- Alternatively, we could deploy the nodes in a smaller area.
- tl;dr: If you want to increase the coverage, you should do at least one of the following: increase the amount of nodes, shrink the deployment area, or ensure that the amount of keys `r` is equal to the square root of `P`, to ensure the birthday paradox happens.

## Conclusion

Sensor networks are very constrained devices, and their limitations make them very hard to deploy in a practical solution. Personally I do not see a practical solution that could make use of a sensor network, either. While it is an interesting concept, I believe that the world is going into a direction in which nanocomputers are going to be more and more powerful, perhaps then a scheme like this could have an application.

\pagebreak

## Appendix A: Graph theory trivia

The amount of edges in a complete graph is `n(n-1)/2`. Proof by intuition: if we have `n` nodes in the network, then every single node can connect with `n-1` other nodes, which gives us `n(n-1)` edges. However, we have to remember that if some node `a` establishes `n-1` edges, then `n-1` nodes also establish a connection with node `a` - this is why we divide by two, because all of the edges are duplicated.

## Appendix B: Example of a generated network

![Sample network establishment for `EG(100, 50, 1024, 32, 10)`.](./img/out.gv.pdf)
