using EschenauerGligor.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EschenauerGligor.Factories
{
    public static class NodeNetworkFactory
    {
        private static readonly Random Random = new Random();

        /// <param name="N">The amount of nodes to generate.</param>
        /// <param name="X">The size of the grid.</param>
        /// <param name="P">Size of the key pool.</param>
        /// <param name="k">Amount of keys to distribute for each node.</param>
        /// <param name="r">Maximum Euclidean distance between nodes.</param>
        public static Node[] Create(int N, int X, int P, int k, int r)
        {
            var keys = GenerateKeyPool(P);
            var coords = GenerateCoords(X, N);

            var nodes = coords.Select(c => new Node(c.X, c.Y, SelectKeyRing(keys, k))).ToArray();
            foreach (var node in nodes) node.FindNeighbours(nodes, r);

            return nodes;
        }

        private static List<(int X, int Y)> GenerateCoords(int size, int amount)
        {
            var hs = new HashSet<(int, int)>();
            while (hs.Count < amount) hs.Add((Random.Next(size), Random.Next(size)));

            return hs.ToList();
        }

        private static List<Key> GenerateKeyPool(int amount)
        {
            return Enumerable.Range(0, amount).Select(i => new Key(i)).ToList();
        }

        private static List<Key> SelectKeyRing(IList<Key> keys, int amount)
        {
            var hs = new HashSet<Key>();
            while (hs.Count < amount) hs.Add(keys[Random.Next(keys.Count)]);

            return hs.ToList();
        }
    }
}