namespace EschenauerGligor.Models
{
    public class Key
    {
        public int Id { get; }

        public Key(int id)
        {
            Id = id;
        }
    }
}