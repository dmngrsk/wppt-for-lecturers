using System;
using System.Collections.Generic;
using System.Linq;

namespace EschenauerGligor.Models
{
    public class Node : IEquatable<Node>
    {
        private static readonly Random Random = new Random();

        public string Id { get; }
        public int X { get; }
        public int Y { get; }
        public IReadOnlyCollection<Key> Keys { get; }
        public IReadOnlyCollection<Node> Neighbours { get; private set; }

        public Node(int x, int y, IReadOnlyCollection<Key> keys)
        {
            Id = RandomString(16);
            X = x;
            Y = y;
            Keys = keys;
        }

        public void FindNeighbours(IReadOnlyCollection<Node> nodes, double threshold)
        {
            Neighbours = nodes
                .Where(node => !Equals(node, this))
                .Where(node => DistanceTo(node) < threshold)
                .Where(node => node.Keys.Intersect(Keys).Any())
                .ToArray();
        }

        public double DistanceTo(Node other)
        {
            return Math.Sqrt(Math.Pow(X - other.X, 2) + Math.Pow(Y - other.Y, 2));
        }

        public bool Equals(Node other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && X == other.X && Y == other.Y && Equals(Keys, other.Keys) && Equals(Neighbours, other.Neighbours);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Node) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ (Keys != null ? Keys.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }
}