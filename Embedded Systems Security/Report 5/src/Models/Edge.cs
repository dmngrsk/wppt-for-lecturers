using System;
using System.Collections.Generic;

namespace EschenauerGligor.Models
{
    public class Edge : IEquatable<Edge>
    {
        public Node Node1 { get; }
        public Node Node2 { get; }

        public Edge(Node n1, Node n2)
        {
            Node1 = n1;
            Node2 = n2;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Edge)) return false;
            return Equals((Edge) obj);
        }

        public bool Equals(Edge other)
        {
            var (t1, t2) = string.CompareOrdinal(Node1.Id, Node2.Id) < 0 ? (Node1, Node2) : (Node2, Node1);
            var (o1, o2) = string.CompareOrdinal(other.Node1.Id, other.Node2.Id) < 0 ? (other.Node1, other.Node2) : (other.Node2, other.Node1);

            return t1.Equals(o1) && t2.Equals(o2);
        }

        public override int GetHashCode()
        {
            var (t1, t2) = string.CompareOrdinal(Node1.Id, Node2.Id) < 0 ? (Node1, Node2) : (Node2, Node1);

            var hashCode = 1001471489;
            hashCode = hashCode * -1521134295 + EqualityComparer<Node>.Default.GetHashCode(t1);
            hashCode = hashCode * -1521134295 + EqualityComparer<Node>.Default.GetHashCode(t2);
            return hashCode;
        }
    }
}