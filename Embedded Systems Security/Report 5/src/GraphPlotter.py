from graphviz import Source
from sys import argv

dot = Source(open(argv[1]).read(), engine='neato')
dot.render(argv[2], view=True)
