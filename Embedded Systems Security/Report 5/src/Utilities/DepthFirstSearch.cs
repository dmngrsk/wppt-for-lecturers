using EschenauerGligor.Models;
using System.Collections.Generic;

namespace EschenauerGligor.Utilities
{
    public static class DepthFirstSearch
    {
        public static IReadOnlyCollection<Node> Search(Node node)
        {
            var result = new List<Node>();
            SearchRecursive(node, result);

            return result;
        }

        private static void SearchRecursive(Node node, ICollection<Node> visited)
        {
            if (visited.Contains(node)) return;
            visited.Add(node);

            foreach (var neighbour in node.Neighbours)
            {
                SearchRecursive(neighbour, visited);
            }
        }
    }
}