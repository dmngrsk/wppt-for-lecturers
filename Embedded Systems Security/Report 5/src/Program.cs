﻿using EschenauerGligor.Builders;
using EschenauerGligor.Factories;
using EschenauerGligor.Models;
using EschenauerGligor.Utilities;
using System;
using System.Linq;

namespace EschenauerGligor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var N = int.Parse(args[0]);
            var X = int.Parse(args[1]);
            var P = int.Parse(args[2]);
            var k = int.Parse(args[3]);
            var r = int.Parse(args[4]);

            var network = NodeNetworkFactory.Create(N, X, P, k, r);
            var builder = new GraphvizSourceBuilder();

            foreach (var node in network)
            {
                builder.AddNode(node);
            }

            foreach (var node in network)
            {
                foreach (var neighbour in node.Neighbours)
                {
                    builder.AddEdge(new Edge(node, neighbour));
                }
            }

            var dfsNode = network.First();
            var coverageFromRandomNode = DepthFirstSearch.Search(dfsNode).Count;

            Console.WriteLine($"// Accessable nodes from ({dfsNode.X}, {dfsNode.Y}): {coverageFromRandomNode} / {N}.\n");
            Console.Write(builder.Build());
        }
    }
}
