﻿using EschenauerGligor.Models;
using System.Collections.Generic;
using System.Text;

namespace EschenauerGligor.Builders
{
    public class GraphvizSourceBuilder
    {
        private readonly HashSet<Node> _nodes;
        private readonly HashSet<Edge> _edges;

        public GraphvizSourceBuilder()
        {
            _nodes = new HashSet<Node>();
            _edges = new HashSet<Edge>();
        }

        public GraphvizSourceBuilder AddNode(Node n)
        {
            _nodes.Add(n);
            return this;
        }

        public GraphvizSourceBuilder AddEdge(Edge e)
        {
            _edges.Add(e);
            return this;
        }

        public string Build()
        {
            var sb = new StringBuilder();
            sb.AppendLine("graph {");

            foreach (var node in _nodes)
            {
                sb.AppendLine($"\t{node.Id} [shape=circle label=\"({node.X}, {node.Y})\" pos=\"{node.X},{node.Y}!\"];");
            }

            foreach (var edge in _edges)
            {
                sb.AppendLine($"\t{edge.Node1.Id} -- {edge.Node2.Id}");
            }

            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}
