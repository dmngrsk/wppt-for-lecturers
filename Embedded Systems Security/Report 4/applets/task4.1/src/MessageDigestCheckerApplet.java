package task1;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.Util;
import javacard.security.CryptoException;
import javacard.security.MessageDigest;

public class MessageDigestCheckerApplet extends Applet {
    private static final byte[] availableAlgorithms = new byte[] {
        MessageDigest.ALG_NULL,
        MessageDigest.ALG_SHA,
        MessageDigest.ALG_MD5,
        MessageDigest.ALG_RIPEMD160,
        // MessageDigest.ALG_224,
        // MessageDigest.ALG_256,
        // MessageDigest.ALG_384,
        // MessageDigest.ALG_512,
        // MessageDigest.ALG3_224,
        // MessageDigest.ALG3_256,
        // MessageDigest.ALG3_384,
        // MessageDigest.ALG3_512,
    };

    private static final byte MDC_CLA = (byte) 0x80;
    private static final byte MDC_INS_CHK = (byte) 0x11;
    private static final byte MDC_INS_DGST = (byte) 0x22;

    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new MessageDigestCheckerApplet().register();
    }
    
    public void process(APDU apdu) {
        if (selectingApplet()) return;
        
        byte[] buffer = apdu.getBuffer();
        byte CLA = (byte) (buffer[ISO7816.OFFSET_CLA] & 0xFF);
        byte INS = (byte) (buffer[ISO7816.OFFSET_INS] & 0xFF);
        byte P1 = (byte) (buffer[ISO7816.OFFSET_P1] & 0xFF);

        if (CLA != MDC_CLA) {
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        }

        switch (INS) {
            case MDC_INS_CHK:
                getAlgorithms(apdu); break;
            case MDC_INS_DGST:
                getDigest(P1, apdu); break;
            default:
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }

    private void getAlgorithms(APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        short length = (short) availableAlgorithms.length;

        Util.arrayCopyNonAtomic(availableAlgorithms, (short) 0, buffer, (short) 0, length);
        apdu.setOutgoingAndSend((short) 0, length);
    }

    private void getDigest(byte algorithm, APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        short inLength = (short) buffer[ISO7816.OFFSET_LC];
        short outLength = (short) buffer[(short) (ISO7816.OFFSET_CDATA + buffer[ISO7816.OFFSET_LC])];
        MessageDigest.OneShot dig = null;

        try {
            dig = MessageDigest.OneShot.open(algorithm);
            dig.doFinal(buffer, (short) ISO7816.OFFSET_CDATA, inLength, buffer, (short) 0);
            apdu.setOutgoingAndSend((short) 0, (short) outLength);
        } catch (CryptoException ce) {
            buffer[0] = (byte) ce.getReason();
            apdu.setOutgoingAndSend((short) 0, (short) 1);
            throw ce;
        } finally {
            if (dig != null) {
                dig.close();
                dig = null;
            }
        }
    }
}