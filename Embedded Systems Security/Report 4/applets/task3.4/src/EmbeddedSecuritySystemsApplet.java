package task4;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.Util;

public class EmbeddedSecuritySystemsApplet extends Applet {
    private static final byte[] data = { 
        (byte) 'E', (byte) 'm', (byte) 'b', (byte) 'e', 
        (byte) 'd', (byte) 'd', (byte) 'e', (byte) 'd', 
        (byte) ' ', (byte) 'S', (byte) 'e', (byte) 'c', 
        (byte) ' ', (byte) 'S', (byte) 'y', (byte) 's', 
        (byte) 't', (byte) 'e', (byte) 'm', (byte) 's'
    };

    private static final byte ESS_CLA = (byte) 0x80;
    private static final byte ESS_INS_ESS = (byte) 0x12;
    private static final byte ESS_INS_REV = (byte) 0x21;

    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new EmbeddedSecuritySystemsApplet().register();
    }
    
    public void process(APDU apdu) {
        if (selectingApplet()) return;

        byte[] buffer = apdu.getBuffer();
        byte CLA = (byte) (buffer[ISO7816.OFFSET_CLA] & 0xFF);
        byte INS = (byte) (buffer[ISO7816.OFFSET_INS] & 0xFF);

        if (CLA != ESS_CLA) {
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        }

        switch (INS) {
            case ESS_INS_ESS:
                sendDataFromMemory(apdu); break;
            case ESS_INS_REV:
                sendDataFromBuffer(apdu); break;
            default:
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }

    private void sendDataFromMemory(APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        short length = (short) data.length;

        Util.arrayCopyNonAtomic(data, (short) 0, buffer, (short) 0, length);
        apdu.setOutgoingAndSend((short) 0, length);
    }
    
    private void sendDataFromBuffer(APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        byte[] reverse = new byte[buffer[ISO7816.OFFSET_LC]];
        short length = (short) reverse.length;

        for (short i = 0; i < length; ++i) {
            short j = (short) ((short) (ISO7816.OFFSET_CDATA - 1) + (short) (length - i));
            reverse[i] = buffer[j];
        }
        
        Util.arrayCopyNonAtomic(reverse, (short) 0, buffer, (short) 0, length);
        reverse = null;

        apdu.setOutgoingAndSend((short) 0, length);
    }
}