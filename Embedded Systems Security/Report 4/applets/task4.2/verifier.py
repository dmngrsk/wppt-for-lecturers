import random

file = open('default.output', 'r').readlines()
d_bytes = file[-2].split(', ')[6:-2]
e_bytes = file[-1].split(', ')[6:-2]

d = long(''.join(str(x) for x in d_bytes[1:(int(d_bytes[0], 16)+1)]), 16)
dn = long(''.join(str(x) for x in d_bytes[(int(d_bytes[0], 16)+2):]), 16)
e = long(''.join(str(x) for x in e_bytes[1:(int(e_bytes[0], 16)+1)]), 16)
en = long(''.join(str(x) for x in e_bytes[(int(e_bytes[0], 16)+2):]), 16)

m = random.randint(1, dn - 1)
c = pow(m, e, en)
mo = pow(c, d, dn)

print "The generated RSA key pair is valid:", m == mo
