package task2;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.CardException;
import javacard.framework.CardRuntimeException;
import javacard.framework.SystemException;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.Util;
import javacard.security.CryptoException;
import javacard.security.KeyBuilder;
import javacard.security.KeyPair;
import javacard.security.RSAPrivateKey;
import javacard.security.RSAPublicKey;

public class KeyPairGeneratorApplet extends Applet {
    private static final byte KPG_CLA = (byte) 0x80;
    private static final byte KPG_INS_PRV = (byte) 0x11;
    private static final byte KPG_INS_PUB = (byte) 0x22;

    private KeyPair keyPair;

    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new KeyPairGeneratorApplet().register();
    }
    
    public void process(APDU apdu) {
        if (selectingApplet()) return;
        
        byte[] buffer = apdu.getBuffer();
        byte CLA = (byte) (buffer[ISO7816.OFFSET_CLA] & 0xFF);
        byte INS = (byte) (buffer[ISO7816.OFFSET_INS] & 0xFF);

        if (CLA != KPG_CLA) {
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        }

        if (keyPair == null) {
            initializeKeyPair(apdu);
        }

        switch (INS) {
            case KPG_INS_PRV:
                getPrivateKey(apdu); break;
            case KPG_INS_PUB:
                getPublicKey(apdu); break;
            default:
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }

    private void initializeKeyPair(APDU apdu) {
        keyPair = new KeyPair(KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_512);
        keyPair.genKeyPair();
    }

    private void getPrivateKey(APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        RSAPrivateKey key = (RSAPrivateKey) keyPair.getPrivate();

        short expLength = key.getExponent(buffer, (short) 1);
        buffer[0] = (byte) expLength;

        short modLength = key.getModulus(buffer, (short) (expLength + 2));
        buffer[(short) (expLength + 1)] = (byte) modLength;

        short totalLength = (short) ((short) (expLength + modLength) + 2);
        apdu.setOutgoingAndSend((short) 0, totalLength);
    }

    private void getPublicKey(APDU apdu) {
        byte[] buffer = apdu.getBuffer();
        RSAPublicKey key = (RSAPublicKey) keyPair.getPublic();

        short expLength = key.getExponent(buffer, (short) 1);
        buffer[0] = (byte) expLength;

        short modLength = key.getModulus(buffer, (short) (expLength + 2));
        buffer[(short) (expLength + 1)] = (byte) modLength;

        short totalLength = (short) ((short) (expLength + modLength) + 2);
        apdu.setOutgoingAndSend((short) 0, totalLength);
    }
}
