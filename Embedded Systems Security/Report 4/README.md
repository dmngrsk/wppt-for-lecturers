# Embedded Security Systems, report #4: Introduction to Java Cards.

**Author**: Damian Górski.

**Date**: May 27, 2018.

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Embedded%20Systems%20Security/Report%204). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## Java Card emulator installation process

In this section we describe the required steps to install the Java Card emulator. This will basically be a walkthrough of the *Installation* section from the Java Card 3 Platform's documentation, available [*here (click!)*](https://docs.oracle.com/javacard/3.0.5/guide/installation.htm#JCUGC116).

In order to launch Java Card 3 Platform on a Windows machine, we need to install the Java Development Kit (JDK) and MinGW GCC compiler for Windows. I also want to use the CLI for this and the following assignments (or, rather, do not want to use Eclipse), hence I opted out to install Apache Ant manually as well. **Remember to verify the environment variables.** After that, we install the platform with the provided installer. The corresponding download links are available on the documentation's page linked above.

To verify that the installation was performed correctly, we will try to build a HelloWorld sample, as described in [*this section (click!)*](https://docs.oracle.com/javacard/3.0.5/guide/running-helloworld-sample-command-line.htm) from the documentation. For that, we launch two command line prompts: one for `cref`, and one for APDU script execution - we will do that with the `ant all` command. A file `default.output` will be generated in the `applet` directory. If its contents are the same as `HelloWorld.output.expected`'s contents, then our Java Card Platform installation is working as intented.

## Preliminaries about the Java Card

The general Java Card workflow is as follows:

1. The applet is compiled into a `.class` file.
2. The `.class` file is converted into `.cap`, `.jca` and `.exp` files, which are used by the APDU runner. The parameters that are used, contain the AppletID (AID) that will be assigned to our newly converter applet.
3. A script, which contains loading the Java Card applet to the APDU, is scaffolded. That script also contains user communication, which is included in a `.scr` file.
4. The runner loads the applet with given AID to memory and runs the commands given by the user in the aforementioned `.scr` file.

A Java Card reads the commands in a specific way, and it works as a somewhat batch system, in which you send a command, and then get the response back. An APDU command has the following structure: ```CLA|INS|P1|P2|Lc|Nc|Le```, where each term is defined as follows:

- `CLA` - one byte containing the type of command, e.g. interindustry or proprietary,
- `INS` - one byte containing specific command to be handled by the applet,
- `P1`, `P2` - two bytes containing the parameters for `INS`,
- `Lc` - one byte containing the number of bytes in `Nc`.
- `Nc` - `Lc` bytes containing the data to be used by the applet.
- `Le` - one byte containing the maximum number of bytes expected in the answer.

The Java Card parses such command and responses in the following structure: ```Nr|SW1|SW2```, where each term is defined as follows:

- `Lr` - one bytes containing the number of bytes in `Nr`.
- `Nr`- `Lr` bytes containing the response data.
- `SW1, SW2` - the response code sent by the APDU. `90 00` means the request have been processed successfully. A complete list of responses can be found [*here (click!)*](https://www.eftlab.com.au/index.php/site-map/knowledge-base/118-apdu-response-list).

## Running an another HelloWorld sample

In order to run provided sample, we will copy the directory with previous sample, replace the `.java` file, and modify the `build.xml` and `*.opt` files a little bit.

First of all, we get rid of the old sample. Then, we copy-and-paste the sample from the website. However, we need to add a package definition, so that the platform can correctly recognize our class:

```java
package helloworldapplet;
```

Then, modify the build definition used by `ant`. Copy the contents of `build_common.xml` from the samples directory, and replace every occurence of `${sample.name}` to our class name, which is `HelloWorldApplet`.

The code for that applet is included in the directory named `task3.2`.

\pagebreak

## Modifying the HelloWorld sample

Now it's time to modify the HelloWorld sample with our code. First of all, we modify how the `INS` command will be interpreted so it matches the specification:

```java
switch (INS) {
    case ESS_INS_ESS: // 0x12
        sendDataFromMemory(apdu); break;
    case ESS_INS_REV: // 0x21
        sendDataFromBuffer(apdu); break;
    default:
        ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
}
```

In order to print the `Embedded Sec Systems` string, all we have to do is modify the content of the static array that is originally in the class. The substitution is trivial, so I will not comment it here.

A more interesting task is reversing the data contained in the buffer, so we will focus on that. There are two approaches:

- Creating a new byte array with size equal to `Lc`. Then, we save the bytes from the buffer to that array in reversed order. Finally, we send the array as a whole with the `arrayCopyNonAtomic` method. This is the approach I have implemented (sample `task3.4`), because I have thought of an important problem: how do I free the memory allocated to the auxiliary array? After some research, it turns out that setting the reference to `null` does the work.
- Reversing the buffer in place, swapping the order of bytes in indices `[DATA_OFFSET, ..., DATA_OFFSET + DATA_SIZE - 1]`. Then, we send the very same buffer when invoking the `arrayCopyNonAtomic` method in a smart way:

```java
Util.arrayCopyNonAtomic(buffer, DATA_OFFSET, buffer, 0, DATA_LENGTH);
apdu.setOutgoingAndSend(0, DATA_LENGTH);
```

We shift the data to the beginning of the output buffer, and then send the first `DATA_LENGTH` bytes. I believe using `apdu.setOutgoingAndSend(DATA_OFFSET, DATA_LENGTH)` without shifting the array could work as well, but have not tried it.

## Message digest in Java Cards

Now that we understand how Java Cards work, it's time to work with some crypto tools. First of all, let's investigate the possibilities of computing a digest of some message. There is a family of classes dedicated to this talk, which all derive from [`MessageDigest` (click!)](https://docs.oracle.com/javacard/3.0.5/api/javacard/security/MessageDigest.html). The also exists a class `MessageDigest.OneShot`, dedicated to creating a digest for a specified message once only. We create an instance of such class with a static factory method: `MessageDigest.OneShot.open(algorithm)`, where `algorithm` is an enum byte specifying the selected digest. The following digest abstractions are available:

- `ALG_NULL (0x00)`
- `ALG_SHA (0x01)`
- `ALG_MD5 (0x02)`
- `ALG_RIPEMD160 (0x03)`
- `ALG_SHA_224 (?)`
- `ALG_SHA_256 (?)`
- `ALG_SHA_384 (?)`
- `ALG_SHA_512 (?)`
- `ALG_SHA3_224 (?)`
- `ALG_SHA3_256 (?)`
- `ALG_SHA3_384 (?)`
- `ALG_SHA3_512 (?)`

When trying to instantiate all the digests in the `cref` emulator, I was successful only with the first four, because the other contants were not found by the compiler. When trying to actually compute the hash, only SHA-1 returns a valid result (sample `task4.1`):

```apdu
CLA: 80, INS: 22, P1: 01, P2: 00,
Lc: 05, 01, 02, 03, 04, 05,
Le: 14, 11, 96, ..., SW1: 90, SW2: 00

CLA: 80, INS: 22, P1: 02, P2: 00,
Lc: 05, 01, 02, 03, 04, 05,
Le: 01, 03, SW1: 6f, SW2: 00

CLA: 80, INS: 22, P1: 03, P2: 00,
Lc: 05, 01, 02, 03, 04, 05,
Le: 01, 03, SW1: 6f, SW2: 00
```

It turns out that the only valid hashing function to use in the `cref` emulator is SHA-1. Shame.

## Public-key encryption in Java Cards

The last point in our menu is the [`KeyPair` (click!)](http://www.win.tue.nl/pinpasjc/docs/apis/jc222/javacard/security/KeyPair.html) class family. Similarly to the message digest, this base class allows for multiple public key schemes. However, our point of interest is the RSA key pair, so we will focus on that only.

\pagebreak

Generating a key pair and extracting it to its binary form is relatively easy:

```java
KeyPair keyPair = new KeyPair(
    KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_512);
keyPair.genKeyPair();

RSAPublicKey key = (RSAPublicKey) keyPair.getPrivate();
RSAPrivateKey key = (RSAPrivateKey) keyPair.getPrivate();

short expLength = key.getExponent(expBuffer, (short) 0);
short modLength = key.getModulus(modBuffer, (short) 0);
```

The sample `task4.2` generates a public/private key pair ((`e`, `n`), (`d`, `n`)) and exports it in the response body. I have prepared a simple Python script, which verifies that the generated key pair is valid, by performing the text-book RSA encryption/decryption round.

```python
d, dn, e, en = ... # Parse the values from default.output file.

m = random.randint(1, dn - 1)
c = pow(m, e, en)
mo = pow(c, d, dn)

print "The generated RSA key pair is valid:", m == mo
```

An another issue with the `cref` emulator is that it allows to generate 512-bit long RSA keys. Shame.

## Conclusion

The laboratory assignments have taught me something about Java Cards, and that they provide some crypto functionalities. However, I am not impressed by the quality of these functionalities, which are probably limited because of how the `cref` emulator works. However, providing that the actual Java Cards can handle the good things correctly, it is good to know that you can rely on these little chips that reside in them.