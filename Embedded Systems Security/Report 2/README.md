# Embedded Security Systems, report #2: Reverse engineering an IrDA protocol

**Author**: Damian Górski.

**Date**: March 24, 2018.

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Embedded%20Systems%20Security/Report%202). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## Reverse engineering the IrDA protocol

The students were given some Saleae samples, which describe digital signals captured from an IrDA receiver, to fiddle with in order to find out the way data is transmitted via infrared. The following section is about the [NEC Infrared Transmission Protocol (click!)](http://techdocs.altium.com/display/FPGA/NEC+Infrared+Transmission+Protocol), which is used in the remote control that produced given samples, and with the knowledge about the protocol, we will dive into the sample analysis.

I want to emphasise that the two figures below are **not** made by me - they belong to [Altium (click!)](http://www.altium.com/), but I am enclosing them in terms of fair use. I will also allow myself to copy some text from their document page descriping the NEC protocol, linked above - let's not reinvent the wheel over again.

First of all, let's introduce the concept of **carrier frequency**, which I understand as a frequency in which unary pulses (shifts from 0 to 1 and vice-versa) are being transmitted. A receiver reads such impulses with given frequency, and then is able to convert it to binary data (as in "I **have received** some signal", and "I **have not received** some signal). The samples that we received is an example of data received via transmission of a signal with carrier frequency 38kHZ, which is used in IR transmission.

How the raw data is treated, is up the programmer to decide. In the provided samples, the authors of the remote decided to use the aforementioned NEC protocol. Every single bit is transmitted as follows (quoting Altium's doc page):

- ```0``` – a 562.5µs pulse burst followed by a 562.5µs space, with a total transmit time of 1.125ms
- ```1``` – a 562.5µs pulse burst followed by a 1.6875ms space, with a total transmit time of 2.25ms

We know how to receive binary data via IR. Now, we can combine raw bits into a message. A **message frame** in the NEC protocol is defined as follows (quoting Altium's doc page):

\pagebreak

- a 9ms leading pulse burst
- a 4.5ms space
- the 8-bit address for the receiving device
- the 8-bit logical inverse of the address
- the 8-bit command
- the 8-bit logical inverse of the command
- a final 562.5µs pulse burst to signify the end of message transmission.

![Message frame](./img/fig1.png)

A message is always 67.5ms in length, because we always include an address, a command **and their negations**, which gives us 16 ```0```s and 16 ```1```s. What if one wants to send a command for more than that duration? We could either resend the message, or use something called a **repeat code**, which is a short message consisting of (quoting Altium's doc page):

- regular 9ms leading pulse burst
- a 2.25ms space
- a 562.5µs pulse burst to mark the end of the space (and hence end of the transmitted repeat code).

![Repeat signal](./img/fig2.png)

So for example, in order to send a message for a second, we will send the internal message first, and then keep sending a repeat code in intervals of 108ms (in 1000-108 = 892ms we will send 9 repeat codes).

\pagebreak

This sums up the NEC protocol. Now, with that knowledge, we can proceed to analyzing the samples data, which is as follows:

|      Sample name      |                 Content                 |
|-----------------------|-----------------------------------------|
|```sample3.logicdata```|```01001100 10110011 11001001 00110110```|
|```sample4.logicdata```|```01001100 10110011 01001011 10110100```|

We conclude that:

- the address of the recipient device is ```01001011``` (inverse: ```10110011```),
- the code of the first button is ```11001001``` (inverse: ```00110110```),
- the code of the second button is ```11001001``` (inverse: ```00110110```).

Unfortonately, with no knowledge of the remote's design, we cannot conclude which button code maps to which command on the remote.

## Reproducing remote's signal with an Arduino Uno and infrared LED

With the knowledge, now we're able to reproduce the signal from given samples. In order to achieve that, we need an infrared transmitter (an IR LED), and a microcontroller (an Arduino Uno). There is plenty of ready implementations for the IR transmission made by the great open-source community, and we will use some of that for our convenience: I selected the [IRremote (click!)](https://github.com/z3t0/Arduino-IRremote) library.

The usage of the IRremote library is straight-forward - we connect an IR LED to an interrupt pin on Arduino (for Uno, that's pin 3), and then use the following API:

```c
#include <IRremote.h>
IRsend irsend;

irsend.sendNEC(msg, msg_len);
irsend.sendRaw(signalArray, signalArray_len, frequency);
```

Constructing and sending the message is easy with use of ```sendNEC``` method, however we unfortunately have to deal with repeat signals by hand with use of ```sendRaw``` method and some boring calculations of the time in-between signals.

I decided to wrap these calls into my own function ```sendRepeatedNEC``` that takes an address, a command code and the duration of the message - one could think of it as holding the button for given amount of milliseconds. It is implemented as follows (full source available in the repository linked at the beginning of this document):

\pagebreak

```c
void sendRepeatedNEC(
    unsigned long addr,
    unsigned long code,
    unsigned long duration)
{
    duration *= 1000;

    unsigned long message =
        (addr << 24) | ((addr ^ 0xFF) << 16) | (code << 8) | (code ^ 0xFF);

    unsigned int repeat[3] = { 9000, 2250, PULSE_LENGTH };

    irsend.sendNEC(message, 32);
    delayMicrosecondsExact(
        REPEAT_INTERVAL_LENGTH - (PULSE_LENGTH * 96L + 13500L));

    while (duration > REPEAT_INTERVAL_LENGTH)
    {
        irsend.sendRaw(repeat, 3, 38);
        delayMicrosecondsExact(
            REPEAT_INTERVAL_LENGTH - (repeat[0] + repeat[1] + repeat[2]));

        duration -= REPEAT_INTERVAL_LENGTH;
    }
}
```

## Comparing the output with samples

I have got a clone of the Saleae Analyzer, so I decided to sample the output of my code and compare it with the samples I have received for decoding.

The results of sample comparison were very satisfying (**yellow**: received samples, **red**: my output):

![Comparison of output and given samples](./img/comparison.png)

While the yellow (receiver's) state is equal to 0 (as in **receiving** input), the red (transmitter's) state is oscilatting between 0 and 1 with a 38kHz carrier frequency (as in **transmitting**), and while when yellow state is equal to 1 (as in **not receiving** input), the red state is equal to 0 (as in **not transmitting**). The duration of the sample between A1 and A2 marks on both lines is roughly 108ms, which a difference of ~0.2ms, which is negligible.

The following figure represents the timeline of samples I produced (with the use of ```sendRepeatedNEC``` function, I transmitted ```sample3``` for 500ms, then took 1000ms of break, and finally transmitted ```sample4``` for 500ms):

![Comparison of output and given samples](./img/timeline.png)

We can see that each of the message intervals between repeat codes oscillates around ~108ms, which is matching what's defined in the NEC protocol.

The samples I produced are available on the git repository linked at the beginning of this document.

## Summary

While preparing this report, I learned how IR transmission works, decoded the NEC protocol, and managed to reproduce it successfully (?).

I believe the task was a sufficient introduction to what awaits me, which is the IR transmission of a given message between two parties, secured by a mutual key. I am looking forward to working it out!
