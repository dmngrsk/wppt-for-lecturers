#include <IRremote.h>
IRsend irsend;

const unsigned long PULSE_LENGTH = 570L;
const unsigned long REPEAT_INTERVAL_LENGTH = 108000L;

const byte ADDR = 0b01001100;
const byte CODE3 = 0b11001001;
const byte CODE4 = 0b01001011;

inline void delayMicrosecondsExact(unsigned long duration)
{
    delay(duration / 1000);
    delayMicroseconds(duration % 1000);
}

/*
 * Creates a NEC message, according to the standard ((addr)(~addr)(code)(~code)), and then sends it via IR LED.
 * If given duration is longer than the duration of a repeat interval, then sends repeat codes according to that duration.
 */
void sendRepeatedNEC(unsigned long addr, unsigned long code, unsigned long duration)
{
    duration *= 1000;
    
    unsigned long message = (addr << 24) | ((addr ^ 0xFF) << 16) | (code << 8) | (code ^ 0xFF);
    unsigned int repeat[3] = { 9000, 2250, PULSE_LENGTH };

    irsend.sendNEC(message, 32);
    delayMicrosecondsExact(REPEAT_INTERVAL_LENGTH - (PULSE_LENGTH * 96L + 13500L));
    
    while (duration > REPEAT_INTERVAL_LENGTH)
    {
        irsend.sendRaw(repeat, 3, 38);
        delayMicrosecondsExact(REPEAT_INTERVAL_LENGTH - (repeat[0] + repeat[1] + repeat[2]));

        duration -= REPEAT_INTERVAL_LENGTH;
    }
}

void setup()
{
}

void loop()
{
    sendRepeatedNEC(ADDR, CODE3, 500);
    delay(1000);
    sendRepeatedNEC(ADDR, CODE4, 500);
    delay(1000);
}
