# Embedded Security Systems, report #3: Securing IrDA transmission with A5/1.

**Author**: Damian Górski.

**Date**: April 8, 2018.

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Embedded%20Systems%20Security/Report%203). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## Goal of the assignment

As seen in the previous assignment, IrDA transmission can be easily intercepted and decoded for a third party's use. Today we will focus on making the transmission more secure. In order to achieve that, we will build a prototype of a transmission link between two Arduinos, with the message being encrypted by an A5/1 stream cipher.

The students were split into groups of four, and then assigned into two teams. Our team members are distributed as follows:

**Team red** (encryption and transmission): Wojciech Kus, Adam Połubek.

**Team blue** (receiving and decryption): Damian Górski, Tomasz Rokita.

\pagebreak

## Slave machine overview

Our task, as the team blue members, was to implement a *slave machine* with the ability to retrieve data transmissted by the *master machine*, and decrypt it. Before we get into the details, we describe the machine workflow in a more abstract way. We also leave the implementation details of the master device to our folks at team red.

As we know, Arduino has a `loop()` function that executes endlessly, and some global memory to play with. We decided to take advantage of that for our convenience:

```c
const short BUFFER_SIZE = 512; // Arduino's memory is limited.
byte KEY_BUFFER[BUFFER_SIZE];
byte CIPHER_BUFFER[BUFFER_SIZE];
char MESSAGE_BUFFER[BUFFER_SIZE];

unsigned long keyOffset = 0;
unsigned long cipherOffset = 0;
unsigned long messageOffset = 0;

void loop()
{
    ReadKeyByte();
    ReadCipherByte();
    DecodeMessageByte();
}
```

This very simple workflow is as follows:

- `ReadKeyByte` waits for a byte from the specified key source and saves it to the cyclic buffer.
- `ReadCipherByte` waits for byte from the specified cipher source and saves it to the cyclic buffer.
- `DecodeMessageByte` checks for presence of both key and cipher bytes at given positions and crosses them in order to retrieve a plaintext byte.

We fully exploit the `loop()` function with three busy-waiting routines that will execute their work when possible. This could be implemented in a much cleaner way using C++'s object oriented programming paradigm (I believe you can do OOP on Arduino), but this structural contract is much easier to implement for a C++ newbie, so we will stick with that.

\pagebreak

## Variant 1: mutual key used by both parties

![Transmission link.](./img/variant1.png)

In this transmission link, the ciphered message is sent via IrDA, and the keystream is sent via UART by the same implementations of A5/1 with same initialization vectors. We once again use the open-source `IRremote` library in order to work with infrared data.

The implementation of `ReadKeyByte` reads a byte from UART, which is fed by an external process. We prepared a short node.js script that reads the keystream from a file generated by A5/1 (report 1), and then outputs it for the Arduino to read. We assume a baud rate of 9600 on both devices (PC, Arduino). Arduino can read the key with the use of `Serial.read()` method, byte by byte. We also enforce dual communication with the external process by sending an ACK message every time a key byte is successfully saved. We store that byte in a cyclic buffer dedicated to key storage.

```c
if (Serial.available() > 0)
{
    byte keyByte = Serial.read();
    
    // Acknowledgement message for the A5/1 process.
    Serial.println("ACK");

    KEY_BUFFER[keyOffset++ % BUFFER_SIZE] = keyByte;
}

```

\pagebreak

The implementation of `ReadCipherByte` reads a byte with the use of IR receiver. We store that byte in a cyclic buffer dedicated to cipher storage. The cipher byte is sent with use of the NEC protocol, which we have already covered (that means 32-bit message is sent for every corresponding byte).

```c
if (irrecv.decode(&results))
{
    if (VerifyMessage(results.value))
    {
        byte cipherByte = (results.value & 0x0000FF00) >> 8;
        CIPHER_BUFFER[cipherOffset++ % BUFFER_SIZE] = cipherByte;
    }
        
    irrecv.resume();
}
```

This is also the step where we verify the message's integrity, and if the target address is the same as ours:

```c
if ((address ^ addressRev) != 0xFF) return false;
if ((code ^ codeRev) != 0xFF) return false;
if ((msg & 0xFFFF0000) != ADDRESS) return false;
```

The implementation of `DecodeMessageByte` is trivial and already described in the previous section.

\pagebreak

## Variant 2: key transmitted in parallel with the ciphertext

![Dual-transmission link.](./img/variant2.png)

The problem with the previous variant is that we have to synchronize both UARTs to use different IVs for every encryption, and the same on both machines while at it, and it could be problematic. This time, we improve our transmission by adding an extra medium to transfer data over it. The ciphered message is sent via UART link (as opposed to IrDA, as in previous variant), and the keystream is sent via IrDA. The retrieval of the keystream via an external process is done only on the master device.

An interesting thing is that the receiver code has not changed almost at all, because the slave device has no knowledge where the data in the UART link comes from, so there is no configuration necessary - we just use the `Serial.read()` method again.

The implementation of `ReadKeyByte` reads a byte in the same manner as variant first's `ReadCipherByte`, but stores the byte in the key cyclic buffer, and vice versa. We are also not sending the ACK code anymore.

The implementation of `DecodeMessageByte` is trivial and already described two sections ago.

\pagebreak

## Summary

There is a lot of place for improvement for the presented transmission links, such as acknowledgement codes between the two communicating Arduinos, forwarding the ciphertext in a better way, or aforementioned object-oriented design of the slave device, however in my opinion that was not the main goal of the task. We implemented a layer of security for a very weak security-wise transmission protocol, that is IrDA.

It is worth noting that a third-party can still easily retrieve the ciphertext (variant 1) or the encryption key (variant 2) by eavesdropping. Assuming that the serial port is safe to use in given situation, and that we are wrongly going to use the same IV for A5/1, variant 2 improves the security and reliability of the link drastically, because the third party will intercept the same key every time it eavesdroppes, and the key with no corresponding ciphertext is basically useless. In variant 1, we could cross every pairwise different ciphertext in order to retrieve the plaintext with a little bit of cryptoanalysis.

While this assignment was a nice exercise to tinker with, I could not help but think of the practical aspect of such transmission link, and I found some contradictions. I am pretty sure that UART is not secure; we just connected two Arduinos with some cables, so if an attacker has physical access to them, he could easily connect something like an logic analyser to that link in order to retrieve data (ciphertext). However, if that is not the case, then why would we bother to encrypt our messages to begin with? And with that question, that I am yet to answer, I conclude my report.

