#include <IRremote.h>

#define NEC_BITS 32
#define ADDRESS 0x00FF0000

const short RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results results;

const short BUFFER_SIZE = 512;
byte KEY_BUFFER[BUFFER_SIZE];
byte CIPHER_BUFFER[BUFFER_SIZE];
char MESSAGE_BUFFER[BUFFER_SIZE];

unsigned long keyOffset = 0;
unsigned long cipherOffset = 0;
unsigned long messageOffset = 0;

bool VerifyMessage(unsigned long msg)
{
    byte address = (msg & 0xFF000000) >> 24;
    byte addressRev = (msg & 0x00FF0000) >> 16;
    byte code = (msg & 0x0000FF00) >> 8;
    byte codeRev = (msg & 0x000000FF);

    if ((address ^ addressRev) != 0xFF) return false;
    if ((code ^ codeRev) != 0xFF) return false;
    if ((msg & 0xFFFF0000) != ADDRESS) return false;

    return true;
}

void ReadKeyByte()
{
    if (Serial.available() > 0)
    {
        byte keyByte = Serial.read();
        Serial.println("ACK");

        KEY_BUFFER[keyOffset++ % BUFFER_SIZE] = keyByte;
    }
}

void ReadCipherByte()
{
    if (irrecv.decode(&results))
    {
        if (VerifyMessage(results.value))
        {
            byte cipherByte = (results.value & 0x0000FF00) >> 8;
            CIPHER_BUFFER[cipherOffset++ % BUFFER_SIZE] = cipherByte;
        }
        
        irrecv.resume();
    }
}

void DecodeMessageByte()
{
    if (messageOffset < keyOffset && messageOffset < cipherOffset)
    {
        char messageByte = KEY_BUFFER[messageOffset % BUFFER_SIZE] ^ CIPHER_BUFFER[messageOffset % BUFFER_SIZE];
        MESSAGE_BUFFER[messageOffset++ % BUFFER_SIZE] = messageByte;

        if (messageOffset == 0)
        {
            Serial.println(String(MESSAGE_BUFFER));
        }
    }
}

void setup()
{
    Serial.begin(9600);
    irrecv.enableIRIn();
}

void loop()
{
    ReadKeyByte();
    ReadCipherByte();
    DecodeMessageByte();
}

