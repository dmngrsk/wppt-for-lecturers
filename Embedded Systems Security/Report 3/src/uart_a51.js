const SerialPort = require("serialport");
const port = new SerialPort(process.argv[2] || "COM1", { baudRate: 9600 });

const CODE_ACKNOWLEDGED = "ACK";

let key = process.argv[3] || "01001010111101010100101111010101010";

function sendNewChar() {
    const char = String.fromCharCode(parseInt(key.slice(0, 8), 2))
    console.log("Writing char: ", char);
    
    if (key.length) {
        port.write(char, function(err) {
            if (err) console.log('Error on write: ', err.message);
        });
    }
    
    key = key.slice(8);
}

port.on('open', function() {
    console.log(`Port ${port} opened.`);
    setTimeout(sendNewChar, 1000);

    port.on('data', function(data) {
        const msg = data.toString('ascii');

        if (data == CODE_ACKNOWLEDGED) sendNewChar();
    });
});
