# Embedded Security Systems, report #1: Shenanigans with use of VHDL

**Author**: Damian Górski.

**Date**: March 11, 2018 (revisited March 24, 2018).

**Repo directory**: [here (click!)](https://gitlab.com/dmngrsk/wppt-for-lecturers/tree/master/Embedded%20Systems%20Security/Report%201). PDF file generated with [```pandoc``` (click!)](https://github.com/jgm/pandoc).

## IV analysis #1: 'the two LFSR-s'

As a basis for Assignments 1 and 2, the students were provided with some ```.vhd``` (should have been ```.vhdl```!) code samples. The implementation (entities, architectures, and the test bench) of the solution was already explained during the laboratories, so I will allow myself to skip repeating what is already known to us.

The most trivial observation is that setting either LFSR's IV to ```0x0000``` will 'disable' such LFSR. It happens because with such IV, the LFSR is not fed any data, and it's shift operation always yields ```0```. In that case, the output is the latter LFSR's output (if both were fed ```0x0000```, then naturally the output is ```0```).

Other than that, I was not able to come up with anything else, unfortunately.

| ```LFSR1```|```LFSR2``` |     Output     |
|:-----------|:-----------|---------------:|
|```0x0000```| *anything* |  ```LFSR2```   |
| *anything* |```0x0000```|  ```LFSR1```   |
|```0x0000```|```0x0000```|  ```{0}^Inf``` |

## Modifying the code into something A5/1-ish

The main obstacle in modifying the code into LFSR-s used in the A5/1 stream cipher, is the length of each LFSR. The entity accepted only an instance of ```std_logic_vector(15 downto 0)``` as ```data``` input, which is only 16 bits long, while A5/1 uses LFSR-s with lengths of 19, 22 and 23. That's why I decided to
 create a brand-new entity for each of the LFSR-s, resulting in entities ```lfsr19```, ```lfsr22``` and ```lfsr23```:

\pagebreak

```vhdl
entity lfsr19 is
    port (clk, ld : in std_logic;
          data : in std_logic_vector(18 downto 0) := (others => '0');
          r : out std_logic);
end lfsr19;

entity lfsr22 is
    port (clk, ld : in std_logic;
          data : in std_logic_vector(21 downto 0) := (others => '0');
          r : out std_logic);
end lfsr22;

entity lfsr23 is
    port (clk, ld : in std_logic; 
          data : in std_logic_vector(22 downto 0) := (others => '0');
          r : out std_logic);
end lfsr23;
```

Their architectures are just the implementations of corresponding A5/1 LFSR-s:

```vhdl
-- architecture a51_fst of lfsr19
q(18 downto 1) <= q(17 downto 0);
q(0) <= q(18) xor q(17) xor q(16) xor q(13);
r <= q(18);
-- architecture a51_snd of lfsr22
q(21 downto 1) <= q(20 downto 0);
q(0) <= q(21) xor q(20);
r <= q(21);
-- architecture a51_trd of lfsr23
q(22 downto 1) <= q(21 downto 0);
q(0) <= q(22) xor q(21) xor q(20) xor q(7);
r <= q(22);
```

Once we have our LFSR-s ready, it's time to connect them into an output signal. I'm doing that in my test bench, which is a shameless copy of the lecturer's code, modified with just the UUTs and with the addition of a new process - I called it ```writer```. What it does, is reading the output of all UUTs every 10 ns (one cycle), XOR-ing them, saving that into the ```RND``` signal, and then writing ```RND``` to ```output.bin``` file:

```vhdl
for UUT1 : lfsr19 use entity work.lfsr19(a51_fst);
for UUT2 : lfsr22 use entity work.lfsr22(a51_snd);
for UUT3 : lfsr23 use entity work.lfsr23(a51_trd);

UUT1 : lfsr19 port map ( clk => clock, ld => load, data => q19, r => r19 );
UUT2 : lfsr22 port map ( clk => clock, ld => load, data => q22, r => r22 );
UUT3 : lfsr23 port map ( clk => clock, ld => load, data => q23, r => r23 );

writer : process
    variable l : line;
    file bin : text open write_mode is "output.bin";
begin
    wait until clock'event and clock = '0';
    rnd <= r19 xor r22 xor r23;

    write(l, std_logic'image(rnd)(2));
    writeline(bin, l);

    wait until clock'event and clock = '1';
end process;
```

## NIST tests

The ```output.bin``` file, containing the result sequence of our pseudo-A5/1, is used in my choice of [NIST tests (click!, sections 2.1, 2.3, 2.15)](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-22r1a.pdf). My language of choice was R - the tool reads the sequence of zeroes and ones produced by the test bench from ```output.bin```, and then processes it according to the NIST test standards. It is available on the git repository linked above (file  ```src/nist_tests.r```). I will include only the output of that tool in this report, as the functions used by it are just straight-out implementations of the functions described by NIST in the above document.

## IV analysis #2: pseudo-A5/1

For each of the tests, I tried to find an IV combination that yielded the worst result. The general rule of thumb for analysing each of the tests' result is: the least the result value is, the less random given sequence is (passing threshold is 0.01).

The following subsections describe the input I have given for the corresponding test and output I received back:

### Monobit test

- ```q19 <= "0011001100110011001"```
- ```q22 <= "1001100110011001100110"```
- ```q23 <= "01100110011001100110011"```
- Result: ```0.1470585```.
- Explanation: The total amount of ```0```s in given sequence is much different than the total amount of ```1```s.

```r
> # Test #1: Frequency (Monobit) Test.
> t1 <- nistFrequency(size)
> print(t1)
[1] 0.1470585
> print(t1 >= 0.01)
[1] TRUE
```

### Runs test

- ```q19 <= "0101010101010101010"```
- ```q22 <= "1010101010101010101010"```
- ```q23 <= "10101010101010101010101"```
- Result: ```0.1157787```.
- Explanation: The oscillation in given output is either too slow or too fast.

```r
> # Test #3: Runs Test.
> t3 <- nistRuns(size)
> print(t3)
[1] 0.1157787
> print(t3 >= 0.01)
[1] TRUE
```

### Random excursions variant test

- ```q19 <= "0001110001110001110"```
- ```q22 <= "1110001110001110001110"```
- ```q23 <= "00011100011100011100011"```
- Result: Most of positive indices below ```0.05```, two below ```0.01```.
- Explanation: The output sequence yields disproportionately many ```0```s over ```1```s in a given order (too many ```0```s precede ```1```s), or vice versa.

```r
> # Test #15: Random Excursions Variant Test.
> t15 <- nistRandomExcursionsVariant(size)
> print(t15)
 [1] 0.431923000 0.449062971 0.493510030 0.456642159 0.471474325 0.683091398
 [7] 0.580912421 0.212317161 0.044862271 0.089632865 0.040461836 0.038433930
[13] 0.070613238 0.023628552 0.004540048 0.003612769 0.010777823 0.039559112
> print(all(unlist(lapply(t15, function(x) x >= 0.01))))
[1] FALSE
```
