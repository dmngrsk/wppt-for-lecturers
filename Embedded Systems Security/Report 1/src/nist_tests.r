# Tests based on: https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-22r1a.pdf

zeroVal = 48
oneVal = 49

fname = "output.bin"
finfo = file.info(fname)
fread = file(fname, "rb")
fdata = readBin(fread, integer(), size=1, n = finfo$size, endian="little")
data = fdata[fdata != 10]
size = length(data)


erfc <- function(x) {
    (2 / sqrt(pi)) * (integrate(function(x) exp(-(x^2)), lower = x, upper = Inf)$value)
}

nistFrequency <- function(n) {
    Sn <- sum(data == oneVal) - sum(data == zeroVal)
    Sn <- abs(Sn) / sqrt(n)

    erfc(Sn / sqrt(2))
}

nistRuns <- function(n) {
    p <- sum(data == oneVal) / n
    v <- 0; for (i in 1:(n - 1)) if (data[i] != data[i + 1]) v <- (v + 1)

    erfc(abs(v - 2 * n * p * (1 - p)) / (2 * sqrt(2 * n) * p * (1 - p)))
}

nistRandomExcursionsVariant <- function(n) {
    s <- rep(0, n + 2); for (i in 1:n) s[i + 1] = (sum(data[1:i] == oneVal) - sum(data[1:i] == zeroVal))
    j <- sum(s == 0); j <- if (n %% 2 == 0 & s[n + 1] == 0) (j - 2) else (j - 1)
    xis_ids <- c(-9:-1, 1:9)
    xis_vals <- unlist(lapply(xis_ids, function(x) sum(s == x)))
    xis <- mapply(c, xis_ids, xis_vals, SIMPLIFY = FALSE)

    unlist(lapply(xis, function(xi) erfc(abs(unlist(xi)[2] - j) / sqrt(2 * j * (4 * abs(unlist(xi)[1]) - 2)))))
}


# In all cases, a sequence is considered non-random when the result value is < 0.01.

# Test #1: Frequency (Monobit) Test.
t1 <- nistFrequency(size)
print(t1)
print(t1 >= 0.01)

# Test #3: Runs Test.
t3 <- nistRuns(size)
print(t3)
print(t3 >= 0.01)

# Test #15: Random Excursions Variant Test.
t15 <- nistRandomExcursionsVariant(size)
print(t15)
print(all(unlist(lapply(t15, function(x) x >= 0.01))))