library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;
entity lfsr19 is
    port (clk, ld : in std_logic; data : in std_logic_vector(18 downto 0) := (others => '0'); r : out std_logic);
end lfsr19;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;
entity lfsr22 is
    port (clk, ld : in std_logic; data : in std_logic_vector(21 downto 0) := (others => '0'); r : out std_logic);
end lfsr22;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;
entity lfsr23 is
    port (clk, ld : in std_logic; data : in std_logic_vector(22 downto 0) := (others => '0'); r : out std_logic);
end lfsr23;



architecture a51_fst of lfsr19 is 
    signal q : std_logic_vector(18 downto 0) := (others => '0');
begin
    process(clk, ld, data)
    begin
      if (ld = '1') then
        q <= data;
      elsif (clk'event and clk = '1') then
          q(18 downto 1) <= q(17 downto 0);
          q(0) <= q(18) xor q(17) xor q(16) xor q(13);
      end if;
    end process;
  
    r <= q(18);
end a51_fst;



architecture a51_snd of lfsr22 is 
    signal q : std_logic_vector(21 downto 0) := (others => '0');
begin
    process(clk, ld, data)
    begin
      if (ld = '1') then
        q <= data;
      elsif (clk'event and clk = '1') then
          q(21 downto 1) <= q(20 downto 0);
          q(0) <= q(21) xor q(20);
      end if;
    end process;
  
    r <= q(21);
end a51_snd;



architecture a51_trd of lfsr23 is 
    signal q : std_logic_vector(22 downto 0) := (others => '0');
begin
    process(clk, ld, data)
    begin
      if (ld = '1') then
        q <= data;
      elsif (clk'event and clk = '1') then
          q(22 downto 1) <= q(21 downto 0);
          q(0) <= q(22) xor q(21) xor q(20) xor q(7);
      end if;
    end process;
  
    r <= q(22);
end a51_trd;