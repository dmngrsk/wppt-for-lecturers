library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;
use std.textio.ALL;

entity a51_tb is
end a51_tb;

architecture simple_a51 of a51_tb is
    constant clock_period : time := 10 ns;
    signal clock : std_logic := '0';
    signal q19 : std_logic_vector(18 downto 0) := (others => '0');
    signal q22 : std_logic_vector(21 downto 0) := (others => '0');
    signal q23 : std_logic_vector(22 downto 0) := (others => '0');
    signal load  : std_logic := '0';
    signal r19, r22, r23 : std_logic;
    signal rnd : std_logic;

    component lfsr19
        port (clk, ld : in std_logic; data : in std_logic_vector(18 downto 0) := (others => '0'); r : out std_logic);
    end component;

    component lfsr22
        port (clk, ld : in std_logic; data : in std_logic_vector(21 downto 0) := (others => '0'); r : out std_logic);
    end component;

    component lfsr23
        port (clk, ld : in std_logic; data : in std_logic_vector(22 downto 0) := (others => '0'); r : out std_logic);
    end component;

    for UUT1 : lfsr19 use entity work.lfsr19(a51_fst);
    for UUT2 : lfsr22 use entity work.lfsr22(a51_snd);
    for UUT3 : lfsr23 use entity work.lfsr23(a51_trd);

begin
    UUT1 : lfsr19 port map ( clk => clock, ld => load, data => q19, r => r19 );
    UUT2 : lfsr22 port map ( clk => clock, ld => load, data => q22, r => r22 );
    UUT3 : lfsr23 port map ( clk => clock, ld => load, data => q23, r => r23 );

    init : process
    begin
        load <= '1';
        q19 <= "0011001100110011001";
        q22 <= "1001100110011001100110";
        q23 <= "01100110011001100110011";

        wait until clock'event and clock = '0';

        load <= '0';
        wait;
    end process;

    clocker : process
    begin
        clock <= not clock;
        wait for clock_period / 2;
    end process;

    writer : process
        variable l : line;
        file bin : text open write_mode is "output.bin";
    begin
        wait until clock'event and clock = '0';
        rnd <= r19 xor r22 xor r23;

        write(l, std_logic'image(rnd)(2));
        writeline(bin, l);
        
        wait until clock'event and clock = '1';
    end process;
end simple_a51;